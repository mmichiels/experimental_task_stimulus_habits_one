function [d] = wait_for_MRI_trigger
% Wait

% Abrir puerto serie.
s1 = serial('COM1');

fclose(s1);
% Leer el valor del trigger.
fopen(s1);
PrevStatus = s1.PinStatus;
fclose(s1);

% Comprobar si se recibe el trigger:
go = false;
% disp('Waiting for fMRI trigger...');
while ~go
    % Leer
    fopen(s1);
    CurrStatus = s1.PinStatus;
    fclose(s1);
    
    % En
    if ~strcmp(CurrStatus.DataSetReady,PrevStatus.DataSetReady)
        go = true;
        d = now;
    end
end