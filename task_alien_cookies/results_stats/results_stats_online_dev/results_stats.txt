(                             Source        SS  ddof1  ...  p-GG-corr       np2  eps
0                         num_block  0.000063      1  ...   0.957986  0.000088  1.0
1              is_overtraining_stim  0.000250      1  ...   0.864402  0.000925  1.0
2  num_block * is_overtraining_stim  0.005071      1  ...   0.553060  0.011107  1.0

[3 rows x 10 columns],      response_accuracy num_block is_overtraining_stim  subj_idx
0             0.454545         7                    0         0
1             0.863636         7                    0         1
2             0.681818         7                    0         2
3             0.954545         7                    0         3
4             0.681818         7                    0         4
..                 ...       ...                  ...       ...
127           0.681818         8                    1        28
128           0.500000         8                    1        29
129           0.818182         8                    1        30
130           0.681818         8                    1        31
131           0.727273         8                    1        32

[132 rows x 4 columns])    Contrast  A  B  Paired  ...       Tail     p-unc   BF10    hedges
0  num_block  7  8    True  ...  two-sided  0.953097  0.135  0.007585

[1 rows x 11 columns]               Contrast  A  B  Paired  ...       Tail     p-unc   BF10    hedges
0  is_overtraining_stim  0  1    True  ...  two-sided  0.881516  0.136 -0.015171

[1 rows x 11 columns](                             Source        SS  ddof1  ...  p-GG-corr       np2  eps
0                         num_block  0.001002      1  ...   0.720989  0.004040  1.0
1              is_overtraining_stim  0.001565      1  ...   0.523422  0.012840  1.0
2  num_block * is_overtraining_stim  0.001002      1  ...   0.613149  0.008081  1.0

[3 rows x 10 columns],      outcome_dev_selected num_block is_overtraining_stim  subj_idx
0                0.318182         7                    0         0
1                0.090909         7                    0         1
2                0.136364         7                    0         2
3                0.000000         7                    0         3
4                0.090909         7                    0         4
..                    ...       ...                  ...       ...
127              0.136364         8                    1        28
128              0.454545         8                    1        29
129              0.181818         8                    1        30
130              0.136364         8                    1        31
131              0.136364         8                    1        32

[132 rows x 4 columns])    Contrast  A  B  Paired  ...       Tail     p-unc   BF10    hedges
0  num_block  7  8    True  ...  two-sided  0.676601  0.147  0.042275

[1 rows x 11 columns]               Contrast  A  B  Paired  ...       Tail     p-unc   BF10    hedges
0  is_overtraining_stim  0  1    True  ...  two-sided  0.520984  0.165 -0.052851

[1 rows x 11 columns](                             Source        SS  ddof1  ...  p-GG-corr       np2  eps
0                         num_block  0.001002      1  ...   0.720989  0.004040  1.0
1              is_overtraining_stim  0.001565      1  ...   0.523422  0.012840  1.0
2  num_block * is_overtraining_stim  0.001002      1  ...   0.613149  0.008081  1.0

[3 rows x 10 columns],      outcome_dev_selected_baselined num_block is_overtraining_stim  subj_idx
0                         -0.181818         7                    0         0
1                         -0.409091         7                    0         1
2                          0.036364         7                    0         2
3                         -0.500000         7                    0         3
4                         -0.309091         7                    0         4
..                              ...       ...                  ...       ...
127                       -0.363636         8                    1        28
128                       -0.045455         8                    1        29
129                       -0.318182         8                    1        30
130                       -0.063636         8                    1        31
131                       -0.263636         8                    1        32

[132 rows x 4 columns])    Contrast  A  B  Paired  ...       Tail     p-unc   BF10   hedges
0  num_block  7  8    True  ...  two-sided  0.676601  0.147  0.03163

[1 rows x 11 columns]               Contrast  A  B  Paired  ...       Tail     p-unc   BF10    hedges
0  is_overtraining_stim  0  1    True  ...  two-sided  0.520984  0.165 -0.039541

[1 rows x 11 columns](                             Source        SS  ddof1  ...  p-GG-corr       np2  eps
0                         num_block  0.003522      1  ...   0.561009  0.010670  1.0
1              is_overtraining_stim  0.000141      1  ...   0.826516  0.001524  1.0
2  num_block * is_overtraining_stim  0.009783      1  ...   0.191298  0.052756  1.0

[3 rows x 10 columns],      response_switch num_block is_overtraining_stim  subj_idx
0           0.136364         7                    0         0
1           0.363636         7                    0         1
2           0.318182         7                    0         2
3           0.454545         7                    0         3
4           0.272727         7                    0         4
..               ...       ...                  ...       ...
127         0.318182         8                    1        28
128         0.045455         8                    1        29
129         0.318182         8                    1        30
130         0.318182         8                    1        31
131         0.363636         8                    1        32

[132 rows x 4 columns])    Contrast  A  B  Paired  ...       Tail     p-unc   BF10    hedges
0  num_block  7  8    True  ...  two-sided  0.506071  0.167 -0.075865

[1 rows x 11 columns]               Contrast  A  B  Paired  ...       Tail     p-unc   BF10    hedges
0  is_overtraining_stim  0  1    True  ...  two-sided  0.856487  0.137  0.015162

[1 rows x 11 columns](                             Source        SS  ddof1  ...  p-GG-corr       np2  eps
0                         num_block  0.003522      1  ...   0.561009  0.010670  1.0
1              is_overtraining_stim  0.000141      1  ...   0.826516  0.001524  1.0
2  num_block * is_overtraining_stim  0.009783      1  ...   0.191298  0.052756  1.0

[3 rows x 10 columns],      response_switch_baselined num_block is_overtraining_stim  subj_idx
0                    -4.263636         7                    0         0
1                    -4.036364         7                    0         1
2                    -4.081818         7                    0         2
3                    -3.945455         7                    0         3
4                    -4.127273         7                    0         4
..                         ...       ...                  ...       ...
127                  -4.081818         8                    1        28
128                  -3.354545         8                    1        29
129                  -4.081818         8                    1        30
130                  -4.081818         8                    1        31
131                  -4.036364         8                    1        32

[132 rows x 4 columns])    Contrast  A  B  Paired  ...       Tail     p-unc   BF10    hedges
0  num_block  7  8    True  ...  two-sided  0.506071  0.167 -0.010739

[1 rows x 11 columns]               Contrast  A  B  Paired  ...       Tail     p-unc   BF10    hedges
0  is_overtraining_stim  0  1    True  ...  two-sided  0.856487  0.137  0.002148

[1 rows x 11 columns](                             Source        SS  ddof1  ...  p-GG-corr       np2  eps
0                         num_block  0.041017      1  ...   0.000379  0.348088  1.0
1              is_overtraining_stim  0.006827      1  ...   0.068095  0.106663  1.0
2  num_block * is_overtraining_stim  0.004103      1  ...   0.185507  0.057678  1.0

[3 rows x 10 columns],      rt_switch_cost num_block is_overtraining_stim  subj_idx
0          0.244007         7                    0         0
1          0.303541         7                    0         1
2          0.303962         7                    0         2
3          0.349563         7                    0         3
4          0.296469         7                    0         4
..              ...       ...                  ...       ...
127        0.358737         8                    1        28
128        0.433490         8                    1        29
129        0.245114         8                    1        30
130        0.292779         8                    1        31
131        0.226773         8                    1        32

[132 rows x 4 columns])    Contrast  A  B  Paired  ...       Tail     p-unc    BF10    hedges
0  num_block  7  8    True  ...  two-sided  0.002116  16.678  0.707556

[1 rows x 11 columns]               Contrast  A  B  Paired  ...       Tail    p-unc   BF10    hedges
0  is_overtraining_stim  0  1    True  ...  two-sided  0.01735  2.741  0.412085

[1 rows x 11 columns](                             Source        SS  ddof1  ...  p-GG-corr       np2  eps
0                         num_block  0.043340      1  ...   0.000268  0.362255  1.0
1              is_overtraining_stim  0.028445      1  ...   0.033353  0.142233  1.0
2  num_block * is_overtraining_stim  0.002316      1  ...   0.278938  0.038951  1.0

[3 rows x 10 columns],      rt_switch_cost_baselined num_block is_overtraining_stim  subj_idx
0                    0.026628         7                    0         0
1                    0.089067         7                    0         1
2                    0.004013         7                    0         2
3                    0.036633         7                    0         3
4                   -0.012722         7                    0         4
..                        ...       ...                  ...       ...
127                  0.098077         8                    1        28
128                  0.251320         8                    1        29
129                  0.025556         8                    1        30
130                  0.044231         8                    1        31
131                 -0.029097         8                    1        32

[132 rows x 4 columns])    Contrast  A  B  Paired  ...       Tail     p-unc    BF10    hedges
0  num_block  7  8    True  ...  two-sided  0.003258  11.442  0.587929

[1 rows x 11 columns]               Contrast  A  B  Paired  ...       Tail     p-unc   BF10    hedges
0  is_overtraining_stim  0  1    True  ...  two-sided  0.025234  2.009 -0.491096

[1 rows x 11 columns]