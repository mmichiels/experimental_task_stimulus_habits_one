

# import scipy.io
# import numpy as np
#
# data = scipy.io.loadmat("file.mat")
#
# for i in data:
#     if '__' not in i and 'readme' not in i:
#         np.savetxt(("file.csv"), data[i], delimiter=',')

import os
import scipy.io
import pandas as pd
import numpy as np

mat_raw_dir = os.path.join('datasets', 'luque_et_al_2019', 'Exp1_Raw_data_mat')
csv_raw_dir = os.path.join('datasets', 'luque_et_al_2019', 'Exp1_Raw_data_csv')
csv_dir = os.path.join('datasets', 'luque_et_al_2019', 'Exp1_data_csv')
cols_names = ["num_trial", "num_block", "block_dev_level", "stim_id", "response_accuracy_old", "response_id",
              "response_time", "points_this_trial", "total_points", "response_accuracy", "participant_number",
              "time_pressure", "num_session"]

if not os.path.exists(csv_raw_dir):
    os.mkdir(csv_raw_dir)
if not os.path.exists(csv_dir):
    os.mkdir(csv_dir)

def raw_mat_to_csv():
    list_input_files = os.listdir(mat_raw_dir)
    for i, filename in enumerate(list_input_files):
        name, extension = os.path.splitext(filename)
        _, num_session, time_pressure, participant_number = name.split("_")

        if extension != ".mat":
            break
        print("Converting file {} of {} ({})".format(i, len(list_input_files), filename))
        filepath = os.path.join(mat_raw_dir, filename)
        output_filename = os.path.splitext(filename)[0] + ".csv"
        output_filepath = os.path.join(csv_raw_dir, output_filename)
    
        mat = scipy.io.loadmat(filepath, squeeze_me=True, struct_as_record=False)
        data = mat["DATA"].trial_data
        row_participant_number = [int(participant_number)] * data.shape[0]
        row_time_pressure = [int(time_pressure)] * data.shape[0]
        row_num_session = [int(num_session)] * data.shape[0]
        data = np.column_stack((data, row_participant_number))
        data = np.column_stack((data, row_time_pressure))
        data = np.column_stack((data, row_num_session))
        data = pd.DataFrame(data, columns=cols_names)
        data.drop(columns=["response_accuracy_old"], inplace=True)
    
        data.to_csv(output_filepath)

def aggr_sessions_in_one_file():
    participants_data_raw = {}

    list_input_files = os.listdir(csv_raw_dir)
    for i, filename in enumerate(list_input_files):
        name, extension = os.path.splitext(filename)
        filepath = os.path.join(csv_raw_dir, filename)
        data = pd.read_csv(filepath)

        _, num_session, time_pressure, participant_number = name.split("_")
        if not participant_number in participants_data_raw:
            participants_data_raw[participant_number] = {}
        if not time_pressure in participants_data_raw[participant_number]:
            participants_data_raw[participant_number][time_pressure] = {}
        if not num_session in participants_data_raw[participant_number][time_pressure]:
            participants_data_raw[participant_number][time_pressure][num_session] = data

    for part_number, data in participants_data_raw.items():
        output_filename = "results_participant_{}.csv".format(part_number)
        output_filepath = os.path.join(csv_dir, output_filename)
        part_dfs = []
        for i, data_time_pressure in data.items():
            for j, data_sess in data_time_pressure.items():
                part_dfs.append(data_sess)

        part_data = pd.concat(part_dfs)
        part_data.to_csv(output_filepath)

if __name__ == "__main__":
    # raw_mat_to_csv()
    aggr_sessions_in_one_file()