# TODO: Fit with single or double alpha
# TODO: Check non-hierarchical model by passing is_group_model=False to the instantiation of HDDM
# TODO: Test depends_on is_overtraining_stim
# TODO: install again pandas==1.0.1 instead of 0.25 version used for kabuki gelman_rubin
# TODO: Include responses not in time (timeout responses), how?
# TODO: Load saved model
# TODO: Show RL-HDDM fitting plots by subjs (as in HDDM posterior predictive automatic plot)
# TODO: Change split_by column for is_overtraining stim and try without depends_on
# TODO: Try things like training the model with training blocks to get param t, so then when training the model with
# dev blocks, we fix param t so we force other params to vary more.
# TODO: Test model goodness with cross-validation

# -- Future work --
"""
Separar en brain results (as far as Im aware) el circuito de reward vs loss. podriamos tener alphas separables para
reward vs loss? esto seguro esta hecho en la literatura o no? otra idea seria demostrar que el spaced training
(long term training) da valores mas solidos de learning que el one session training en el modelo. aunque en su
raw data no parece ser muy diferente overall. lo mas diferenciador es la consolidacion a largo plazo de las
asociaciones, que si tiene (como es esperable) un efecto en los spaced trainers.

En conductual sería muy interesante ver qué pasa con los drift rates, alphas y non-decision time cuando comparas
el aprendizaje espaciado y masivo.  Es posible que para estos datos el modelo que mejor se ajuste sea uno con
diferente learning rate para win and loss (eso es lo normal, lo raro es que no le salga así a Wielher).
Si tomamos estos parámetros como regresores, se deberían ver diferentes activaciones para los cambios en drift rate,
alpha etc (alpha más relacionado con goal-directed?) y también diferentes áreas relacionadas con el learning rate
para de win y loss (estó habría que pensar como hacerlo, si con el learning rate o con el PE diferenciando si
en ese ensayo se obtuvo feedback positivo o negativo).
"""


import os

import pymc

# from computational_models import CompModel as comp_model
import hddm
import numpy as np
import pandas as pd
import numpy as np
import seaborn as sns
import plotly
import plotly.graph_objs as plotly_graph
import plotly.figure_factory as plotly_figures
# from IPython.display import Image
# Image(img_bytes)
import plotly.io as pio
# pio.renderers.default = "browser"
from plotly.offline import iplot
import scipy.stats as scipy_stats
import statsmodels.api as sm
import statsmodels.stats.multicomp as sm_multicomp
import statsmodels.formula.api as sm_formulas
import kabuki
from kabuki.analyze import gelman_rubin
from tqdm import tqdm  # progress tracker
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')

def proportion():
    return lambda x: x.sum() / x.count()

def proportion_by_val(val=0, vals_sum=None):
    return lambda x: ((x==val).sum() if vals_sum is None else vals_sum) / x.count()

class RL_HDDM():
    def __init__(self, num_subjs, depends_conditions_by=None, split_by="split_by", dataset_filename=None, data_blocks="training",
                 model_type="RL-HDDM", model_is_hierarchical=True):
        this_dir = os.path.dirname(os.path.abspath(__file__))
        project_dir = os.path.join(this_dir, '..')
        os.chdir(project_dir)

        self.subj_names = []
        self.dir_datasets = os.path.join("computational_models", "datasets")
        self.dir_results_comp_models = os.path.join("computational_models", "results_comp_models")
        if not os.path.isdir(self.dir_datasets):
            os.mkdir(self.dir_datasets)
        if not os.path.isdir(self.dir_results_comp_models):
            os.mkdir(self.dir_results_comp_models)

        self.map_aggregate_trials = {
            "num_trial": "count",
            "response_accuracy": proportion(),
            "response_time": "mean",
            "outcome_selected_id": "mean",
            "outcome_dev_id": "mean",
            "outcome_dev_selected": proportion(),  # Response selection
            "response_switch": proportion(),  # Response selection
        }

        self.model_type = model_type
        self.model_is_hierarchical = model_is_hierarchical
        self.split_by = split_by
        self.depends_conditions_by = depends_conditions_by
        if self.depends_conditions_by == "is_overtraining_stim" or self.split_by == "is_overtraining_stim":
            self.conditions = [0, 1]
        elif self.depends_conditions_by == "stim_id" or self.split_by == "stim_id":
            self.conditions = [0, 1, 2, 3]
        if self.depends_conditions_by is None and self.split_by is not None:
            self.conditions = list(range(3))  # In this case there are no conditions. These are just split by conds
        self.num_conditions = len(self.conditions)

        self.get_filepaths()
        self.results_participants = self.load_data(num_subjs)
        self.drop_invalid_participants(max_consumption_trials_errors_per_block=1)
        self.drop_invalid_trials()  # Like free consumption trials
        self.separate_training_dev_blocks()  # self.results_participants_training, self.results_participants_dev, self.results_agg_dev =

        if dataset_filename is None:
            dataset_filename = "current_aliens_dataset.csv"
            participants_data = self.results_participants_training if data_blocks == "training" else self.results_participants_dev
            self.data_df = self.transform_data_to_input_hddm(participants_data)

        self.data_hddm = self.load_data_in_hddm(dataset_filename)

        # self.l_r = 0.1
        # self.softmax_temp = 3  # Softmax temperature (higher temp -> more exploration; lower temp -> more exploitation)
        #
        # # Block-trials config (every block has num_trials_per_block; Total trials = num_blocks*num_trials_per_block)
        # self.num_blocks = 9
        # self.num_trials_per_block = 44
        # self.num_trials_per_block_dev = 120
        # self.dev_block_every = 0
        # self.consumption_trial_every = 999
        # if self.session_number == 2:
        #     self.num_blocks = 2
        #     self.dev_block_every = 1
        #
        # self.trials_stims_indices = self.calculate_trials_stims_indices(self.num_trials_per_block, per_over_stim=0.5,
        #                                                                 times_over_stim=10, consumption_trials=True)
        # self.trials_stims_indices_dev = self.calculate_trials_stims_indices(self.num_trials_per_block_dev, per_over_stim=0.5,
        #                                                                 times_over_stim=1, consumption_trials=False)

    def drop_invalid_participants(self, max_consumption_trials_errors_per_block):
        # TODO: filter participants by bad performance in free consumptions trials in dev blocks:
        # 1st: get only (filter) free consumptions trials for every participant in all dev blocks (or try in separate too).
        # 2nd: aggregate results to obtain percentage of response_accuracy
        # 3rd: find those participants with response_accuracy lower than X% (90% ?) and drop them.

        results_participants_dev = self.filter_data_by_col_vals(self.results_participants, "outcome_dev_id", [-1],
                                                                inverse=True)
        results_participants_consumption = self.filter_data_by_col_vals(results_participants_dev,
                                                                        "trial_type", [1])
        num_consumption_trials = results_participants_consumption[0].shape[0]
        results_participants_consumption, results_agg_consumption = self.group_data(results_participants_consumption,
                                                                                    ["num_block"])
        num_consumption_trials_per_block = int(num_consumption_trials / results_participants_consumption[0].shape[0])
        threshold_accuracy = (num_consumption_trials_per_block - max_consumption_trials_errors_per_block) / num_consumption_trials_per_block

        participants_remove_indices = []
        # participants_remove_indices = [32]
        for i, partic in enumerate(results_participants_consumption):
            if partic.loc[0, "response_accuracy"] < threshold_accuracy or \
                    partic.loc[1, "response_accuracy"] < threshold_accuracy:
                participants_remove_indices.append(i)

        self.results_participants = [partic for i, partic in enumerate(self.results_participants)
                                     if i not in participants_remove_indices]

        print("Removed {} participants that had more than {} errors in consumption trials in dev blocks".format(
            len(participants_remove_indices), max_consumption_trials_errors_per_block))
        print("--------Total participants for analysis: {}---------------------------".format(
            len(self.results_participants)))

    def drop_invalid_trials(self):
        # Drop free consumption trials and timeout trials (response too fast or too slow)
        self.results_participants = self.filter_data_by_col_vals(self.results_participants,
                                                                 "trial_type", [1], inverse=True)
        # self.results_participants = self.filter_data_by_col_vals(self.results_participants,
        #                                                          "num_block", [8], inverse=True)
        self.results_participants = self.filter_data_by_col_vals(self.results_participants, "response_in_time",
                                                                 [0])

    def group_data(self, results_each_participant, cols_agg=["num_block", "is_overtraining_stim"], count_vals_agg=None,
                   agg_by_participants=True):
        results_participants = []

        for i, results_part in enumerate(results_each_participant):
            if count_vals_agg is not None:
                self.map_aggregate_trials["response_switch"] = proportion_by_val(vals_sum=count_vals_agg[i])
            results_part_agg = results_part
            if agg_by_participants:
                results_part_agg = self.pd_group_and_aggr(results_part, cols_agg)
            results_participants.append(results_part_agg)

        participants_idx = range(len(results_participants))
        results_agg_pd = pd.concat(results_participants, axis=0, keys=participants_idx)
        results_agg_pd = results_agg_pd.groupby(cols_agg).agg(list)

        return results_participants, results_agg_pd

    def pd_group_and_aggr(self, df, cols):
        df = df.groupby(cols).agg(self.map_aggregate_trials).reset_index()

        return df

    def separate_training_dev_blocks(self):
        self.results_participants_dev = self.filter_data_by_col_vals(self.results_participants, "outcome_dev_id", [-1],
                                                                     inverse=True)
        # Preceding training block for baseline dev data:
        self.results_participants_training = self.filter_data_by_col_vals(self.results_participants, "outcome_dev_id",
                                                                          [-1])

    def filter_data_by_col_vals(self, participants_dfs, col, vals=None, vals_per_participant=None, inverse=False):
        results_participants_filtered = []
        for i, results_participant in enumerate(participants_dfs):
            vals_filter = vals if vals_per_participant is None else vals_per_participant[i]
            mask = results_participant[col].isin(vals_filter)
            if inverse:
                mask = ~mask
            results_participant_filtered = results_participant.loc[mask, :]
            results_participants_filtered.append(results_participant_filtered)

        return results_participants_filtered

    def get_filepaths(self):
        # self.dir_results = os.path.join('.', 'results')
        self.dir_results = os.path.join('.', 'results', 'results_v1_fmri_with_rt_switch_cost_with_subjs_names')
        self.results_file_prefix = ""  # "results"
        self.participant_info_file_prefix = "info_"

    def load_data(self, num_subjs):
        results_each_participant = []
        filenames_by_date = os.listdir(self.dir_results)
        filepaths_by_date = [os.path.join(self.dir_results, filename) for filename in filenames_by_date]
        filepaths_by_date.sort(key=lambda x: os.path.getmtime(x))

        for i, filename in enumerate(filenames_by_date):
            filepath = os.path.join(self.dir_results, filename)
            is_csv_results_file = filename.startswith(self.results_file_prefix) and filename.endswith('.csv')
            if is_csv_results_file and "_tmp" not in filename:
                results_participant = pd.read_csv(filepath, sep=',')
                results_each_participant.append(results_participant)
                subj_name = filename.split(".csv")[0]
                self.subj_names.append(subj_name)
                print("File loaded: {}".format(filename))

            if num_subjs is not None and len(results_each_participant) >= num_subjs:
                break

        print("Loading complete. {} files loaded".format(len(results_each_participant)))

        return results_each_participant

    def transform_data_to_input_hddm(self, participants_data):
        # Necessary columns (example):
        # subj_idx,response,cond,rt,trial,split_by,feedback,q_init
        # 42,0,CD,1.255,1,1,0,0.5

        num_participants = len(participants_data)
        df = pd.concat(participants_data, keys=range(num_participants))
        df.reset_index(level=1, inplace=True)
        df["subj_idx"] = df.index

        df["q_init"] = [0.5] * df.shape[0]
        df["feedback"] = df["response_accuracy"].copy()
        df["split_by"] = df[self.split_by].copy()
        # df["split_by"] = df[self.depends_conditions_by].copy()

        cols_map = {
            "response_accuracy": "response",
            "response_time": "rt",
            "num_trial": "trial",
            # "points_this_trial": "feedback", # points_this_trial
        }
        df.rename(columns=cols_map, inplace=True)

        columns_order = ["subj_idx", "response", "split_by", "rt", "trial", "feedback", "q_init", "stim_id", "is_overtraining_stim"]
        df = df.reindex(columns=columns_order)
        df.reset_index(inplace=True, drop=True)

        filepath = os.path.join(self.dir_datasets, "current_aliens_dataset.csv")
        df.to_csv(filepath, index=False)

        return df

    def load_data_in_hddm(self, dataset_filename):
        dataset_file = os.path.join(self.dir_datasets, dataset_filename)
        data_hddm = hddm.load_csv(dataset_file)

        return data_hddm

    def check_params_collinearity(self, model, show_plot=False):
        filepath = os.path.join(self.dir_results_comp_models, 'rl_hddm_collinearity.png')

        alpha, t, a, v = model.nodes_db.node[['alpha', 't', 'a', 'v']]
        samples = {'alpha': alpha.trace(), 't': t.trace(), 'a': a.trace(), 'v': v.trace()}
        samp = pd.DataFrame(data=samples)

        g = sns.PairGrid(samp, palette=["red"])
        g.map_upper(plt.scatter, s=10)
        g.map_diag(sns.distplot, kde=False)
        g.map_lower(sns.kdeplot, cmap="Blues_d")
        g.map_lower(corrfunc)
        g.savefig(filepath)
        if show_plot:
            plt.show()

    def simulate_data(self, model, num_subjs=None, num_simulations=50, show_plots=True, num_trials_show_plot=None):
        # Function to simulate data with the fitted params to compare with the observed data

        filepath_results = os.path.join(self.dir_results_comp_models, "rl_hddm_simulated_data.csv")
        # create empty dataframe to store simulated data
        all_sim_data = pd.DataFrame()
        # create a column samp to be used to identify the simulated data sets
        self.data_hddm['samp'] = 0
        # load traces
        traces = model.get_traces()
        if num_subjs is None:
            num_subjs = len(self.data_hddm.subj_idx.unique())

        # decide how many times to repeat simulation process. repeating this multiple times is generally recommended,
        # as it better captures the uncertainty in the posterior distribution, but will also take some time
        for i in tqdm(range(num_simulations)):
            # randomly select a row in the traces to use for extracting parameter values
            sample = np.random.randint(0, traces.shape[0] - 1)
            # loop through all subjects in observed data
            for s in range(num_subjs):
                if self.depends_conditions_by is None:
                    a = traces.loc[sample, 'a_subj.' + str(s)]
                    t = traces.loc[sample, 't_subj.' + str(s)]
                    scaler = traces.loc[sample, 'v_subj.' + str(s)]
                    if self.model_type != "HDDM":
                        alphaInv = traces.loc[sample, 'alpha_subj.' + str(s)]
                else:
                    a = [traces.loc[sample, 'a_subj({}).'.format(stim) + str(s)] for stim in self.conditions]
                    t = [traces.loc[sample, 't_subj({}).'.format(stim) + str(s)] for stim in self.conditions]
                    scaler = [traces.loc[sample, 'v_subj({}).'.format(stim) + str(s)] for stim in self.conditions]
                    if self.model_type != "HDDM":
                        alphaInv = [traces.loc[sample, 'alpha_subj({}).'.format(stim) + str(s)] for stim in self.conditions]

                if self.model_type != "HDDM":
                    # take inverse logit of estimated alpha
                    alpha = np.exp(alphaInv) / (1 + np.exp(alphaInv))

                sim_data = []
                for cond_idx in range(self.num_conditions):
                    cond = self.conditions[cond_idx]
                    if self.depends_conditions_by is None:
                        col = "split_by"
                        a_this = a
                        t_this = t
                        scaler_this = scaler
                        if self.model_type != "HDDM":
                            alpha_this = alpha
                    else:
                        col = self.depends_conditions_by
                        a_this = a[cond_idx]
                        t_this = t[cond_idx]
                        scaler_this = scaler[cond_idx]
                        if self.model_type != "HDDM":
                            alpha_this = alpha[cond_idx]
                    size = len(self.data_hddm[(self.data_hddm['subj_idx'] == s) & (self.data_hddm[col] == cond)].trial.unique())

                    # simulate data for each condition changing only values of size, p_upper, p_lower and split_by between conditions.
                    if self.model_type != "HDDM":
                        sim_data_cond = hddm.generate.gen_rand_rlddm_data(a=a_this, t=t_this, scaler=scaler_this,
                                                                          alpha=alpha_this, size=size, p_upper=1,
                                                                          p_lower=0,
                                                                          split_by=cond)
                    else:
                        sim_data_cond = hddm.generate.gen_rand_data({cond: {'v':scaler_this, 'a':a_this, 't':t_this}},
                                                                    size=size, subjs=1)[0]
                    sim_data.append(sim_data_cond)

                # append the conditions
                sim_data = pd.concat(sim_data)
                # assign subj_idx
                sim_data['subj_idx'] = s
                # identify that these are simulated data
                sim_data['type'] = 'simulated'
                # identify the simulated data
                sim_data['samp'] = i
                # append data from each subject
                all_sim_data = all_sim_data.append(sim_data, ignore_index=True)
        # combine observed and simulated data
        cols = ['subj_idx', 'response', 'split_by', 'rt', 'trial', 'feedback', 'samp']
        if self.depends_conditions_by is not None:
            cols.append(self.depends_conditions_by)
        ppc_data = self.data_hddm[cols].copy()
        ppc_data['type'] = 'observed'
        if self.model_type != "HDDM":
            ppc_sdata = all_sim_data[['subj_idx', 'response', 'split_by', 'rt', 'trial', 'feedback', 'type', 'samp']].copy()
        else:
            ppc_sdata = all_sim_data[['subj_idx', 'response', 'rt', 'type', 'samp', 'condition']].copy()
            ppc_sdata.rename(columns={'condition': self.depends_conditions_by}, inplace=True)

        col_cond = self.depends_conditions_by if self.depends_conditions_by is not None else "split_by"
        ppc_sdata.rename(columns={'split_by': col_cond}, inplace=True)
        ppc_data = ppc_data.append(ppc_sdata)
        ppc_data.to_csv(filepath_results)

        if show_plots:
            if num_trials_show_plot is None:
                plot_ppc_data = ppc_data.copy()
            else:
                # for practical reasons we only look at the first X trials for each subject in a given condition
                plot_ppc_data = ppc_data[ppc_data.trial < num_trials_show_plot].copy()
            # bin trials to for smoother estimate of response proportion across learning
            plot_ppc_data['bin_trial'] = pd.cut(plot_ppc_data.trial, 11, labels=np.linspace(0, 10, 11)).astype('int64')
            # calculate means for each sample
            cols = ['bin_trial', 'samp', 'type']
            if self.depends_conditions_by is not None:
                cols.append(self.depends_conditions_by)
            sums = plot_ppc_data.groupby(cols).mean().reset_index()
            # calculate the overall mean response across samples
            ppc_sim = sums.groupby(cols).mean().reset_index()

            # self.plot_choice_sim_data(sums, ppc_sim)
            self.plot_rt_sim_data(plot_ppc_data)

    def plot_choice_sim_data(self, sums, ppc_sim):
        filepath_results_csv = os.path.join(self.dir_results_comp_models, 'rl_hddm_choice_data.csv')
        filepath_results_plot = os.path.join(self.dir_results_comp_models, 'rl_hddm_choice_data.svg')

        # initiate columns that will have the upper and lower bound of the hpd
        ppc_sim['upper_hpd'] = 0
        ppc_sim['lower_hpd'] = 0
        for i in range(0, ppc_sim.shape[0]):
            # calculate the hpd/hdi of the predicted mean responses across bin_trials
            hdi = pymc.utils.hpd(sums.response[(sums['bin_trial'] == ppc_sim.bin_trial[i]) & (
                    sums[self.depends_conditions_by] == ppc_sim[self.depends_conditions_by][i]) & (sums['type'] == ppc_sim.type[i])], alpha=0.1)
            ppc_sim.loc[i, 'upper_hpd'] = hdi[1]
            ppc_sim.loc[i, 'lower_hpd'] = hdi[0]
        # calculate error term as the distance from upper bound to mean
        ppc_sim['up_err'] = ppc_sim['upper_hpd'] - ppc_sim['response']
        ppc_sim['low_err'] = ppc_sim['response'] - ppc_sim['lower_hpd']
        ppc_sim['model'] = 'RLDDM_single_learning'
        ppc_sim.to_csv(filepath_results_csv)

        # plotting evolution of choice proportion for best option across learning for observed and simulated data.
        fig, axs = plt.subplots(figsize=(15, 5), nrows=1, ncols=self.num_conditions, sharex=True, sharey=True)
        for i in range(self.num_conditions):
            ax = axs[i]
            d = ppc_sim[(ppc_sim.split_by == i) & (ppc_sim.type == 'simulated')]
            ax.errorbar(d.bin_trial, d.response, yerr=[d.low_err, d.up_err], label='simulated', color='orange')
            d = ppc_sim[(ppc_sim.split_by == i) & (ppc_sim.type == 'observed')]
            ax.plot(d.bin_trial, d.response, linewidth=3, label='observed')
            ax.set_title('split_by ({}) = {}'.format(self.depends_conditions_by, i), fontsize=20)
            ax.set_ylabel('mean response')
            ax.set_xlabel('trial')

        plt.legend()
        fig.savefig(filepath_results_plot)

    def plot_rt_sim_data(self, plot_ppc_data):
        filename_results_plot = 'rl_hddm_rt_data.svg' if self.model_type == "RL-HDDM" else 'hddm_rt_data.svg'
        filepath_results_plot = os.path.join(self.dir_results_comp_models, filename_results_plot)

        # set reaction time to be negative for lower bound responses (response=0)
        plot_ppc_data['reaction time'] = np.where(plot_ppc_data['response'] == 1, plot_ppc_data.rt,
                                                  0 - plot_ppc_data.rt)
        # plotting evolution of choice proportion for best option across learning for observed and simulated data. We use bins of trials because plotting individual trials would be very noisy.
        col = self.depends_conditions_by if self.depends_conditions_by is not None else None
        g = sns.FacetGrid(plot_ppc_data, col=col, hue='type')
        g.map(sns.kdeplot, 'reaction time', bw=0.05).set_ylabels("Density")
        g.add_legend()
        g.savefig(filepath_results_plot)

    def anova(self, data, dep_var, indep_vars):
        data[dep_var] = data[dep_var].astype('float')
        data[indep_vars] = data[indep_vars].astype('category')
        data = data.loc[:, [dep_var]+indep_vars]

        ols_formula_str = '{}  ~ '.format(dep_var)
        ols_interactions_str = ''
        for i, indep_var in enumerate(indep_vars):
            ols_formula_str += 'C({}) + '.format(indep_var)
            if i < len(indep_vars)-1:
                ols_interactions_str += 'C({}):'.format(indep_var)
            else:
                ols_interactions_str += 'C({})'.format(indep_var)
        ols_formula_str += ols_interactions_str

        model_ols = sm_formulas.ols(ols_formula_str, data=data).fit()
        anova_table = sm.stats.anova_lm(model_ols, typ=2)
        # print(model_ols.summary())
        print("ANOVA table:")
        print(anova_table)

        return anova_table, data

    def train_model(self, num_samples=200, alpha_dual=False, p_outlier=None, burn_samples=None, num_models_convergence=3, check_convergence=False, show_plots=False):
        # Load data from csv file into a NumPy structured array
        # dataset_file = os.path.join(self.dir_datasets, "aliens_test_same_response_id.csv")
        # dataset_file = os.path.join(self.dir_datasets, "rlddm_data_single_subj.csv")

        # Create a HDDM model multi object
        # model = hddm.HDDM(self.data_hddm) #, depends_on={'v': 'difficulty'})
        if self.depends_conditions_by is None:
            depends_on_hddm = None
            depends_on_rl_hddm = None
        else:
            depends_on_hddm = {'v': self.depends_conditions_by, 'a': self.depends_conditions_by, 't': self.depends_conditions_by,
                               'alpha': self.depends_conditions_by}
            depends_on_rl_hddm = {'v': self.depends_conditions_by, 'a': self.depends_conditions_by, 't': self.depends_conditions_by,
                                  'alpha': self.depends_conditions_by}

        if self.model_type == "HDDM":
            model = hddm.HDDM(self.data_hddm, depends_on=depends_on_hddm)  # , depends_on={'v': 'difficulty'})
        else:
            if p_outlier is None:
                model = hddm.HDDMrl(self.data_hddm,
                                    depends_on=depends_on_rl_hddm,
                                    dual=alpha_dual)  # , depends_on={'v': 'difficulty'})
            else:
                model = hddm.HDDMrl(self.data_hddm, depends_on=depends_on_rl_hddm,
                                    dual=alpha_dual, p_outlier=p_outlier)  # , depends_on={'v': 'difficulty'})

        if burn_samples is None:
            burn_samples = int(num_samples / 2)

        if not check_convergence:
            # Create model and start MCMC sampling
            # model.find_starting_values()
            model.sample(num_samples, burn=burn_samples)  # 2000
        else:
            # Got convergence with num_models_convergence=3 and num_samples=5000
            models = []
            for i in range(num_models_convergence):
                if self.depends_conditions_by is None:
                    m = hddm.HDDMrl(data=self.data_hddm, dual=alpha_dual)
                else:
                    m = hddm.HDDMrl(data=self.data_hddm, depends_on=depends_on_rl_hddm,
                                    dual=alpha_dual)
                # m.find_starting_values()
                m.sample(num_samples, burn=burn_samples, dbname='traces.db',db='pickle')
                models.append(m)

            gelman_rubin_results = gelman_rubin(models)  # Estimate convergence
            print("Gelman rubin results: {}".format(gelman_rubin_results))
            gelman_rubin_score = np.max(list(gelman_rubin_results.values()))
            print("Gelman rubin best model score: {}".format(gelman_rubin_score))
            # Combine the models we ran to test for convergence.
            model = kabuki.utils.concat_models(models)

        self.model = model
        # Save and print fitted parameters and other model statistics
        params_df = self.create_params_df(model)
        dep_vars_params = ["v", "t", "a"]
        if self.model_type == "RL-HDDM":
            dep_vars_params.append("alpha")

        if not self.model_is_hierarchical:
            self.multi_t_test_in_params(params_df, dep_vars_params)
        else:
            self.multi_bayesian_testing(params_df, dep_vars_params)

        if show_plots:
            # Plot posterior distributions and theoretical RT distributions
            model.plot_posteriors()
            plt.show()

        if self.model_type == "HDDM":
            plt.rcParams["figure.figsize"] = (30, 23)
            plt.figure(figsize=(30, 23))
            model.plot_posterior_predictive()
            plt.gcf().set_size_inches(30, 23)
            # plt.rcParams["figure.figsize"] = (30, 23)
            # plt.figure(figsize=(30, 23))
            filepath_results_hddm_plot = os.path.join(self.dir_results_comp_models, 'hddm_rt_data_by_subjs.svg')
            plt.savefig(filepath_results_hddm_plot, dpi=500, bbox_inches="tight")
            if show_plots:
                plt.show()


        return model

    def create_params_df(self, model, save_file=True):
        params_output_txt_filepath = os.path.join(self.dir_results_comp_models, "comp_model_params_output.txt")
        params_output_csv_filepath = os.path.join(self.dir_results_comp_models, "comp_model_params_output.csv")
        params_output_subjs_csv_filepath = os.path.join(self.dir_results_comp_models, "comp_model_params_output_subjs.csv")

        model.print_stats()  # Console
        model.print_stats(params_output_txt_filepath)  # File

        params_stats = []
        params_subjs_stats = {}
        params_subjs_stats["subj_name"] = self.subj_names * len(self.conditions)
        params_subjs_stats["overtrained"] = [0]*len(self.subj_names) + [1]*len(self.subj_names)
        params_raw_df = pd.read_csv(params_output_txt_filepath)
        cols = ["subj", "param", "mean", "std", "2.5q", "25q", "50q", "75q", "97.5q", "mc err"]
        for i, row in params_raw_df.iterrows():
            all_vals = row.iloc[0].split(' ')
            subj_name = None
            if "." in all_vals[0]:
                subj_idx = int(all_vals[0].split(".")[1])
                subj_name = self.subj_names[subj_idx]

            vals = [subj_name] + [val for val in all_vals if val != ""]
            params_stats.append(vals)

            if subj_name is not None:
                vals = [vals[2], vals[3]]
                param_name = all_vals[0].split("_")[0]
                params_names = [param_name + "__mean", param_name + "__std"]

                for i, name in enumerate(params_names):
                    if name not in params_subjs_stats:
                        params_subjs_stats[name] = [vals[i]]
                    else:
                        params_subjs_stats[name].append(vals[i])

        params_df = pd.DataFrame(params_stats, columns=cols)
        params_subjs_df = pd.DataFrame(params_subjs_stats)

        if save_file:
            params_df.to_csv(params_output_csv_filepath, index=False)
            params_subjs_df.to_csv(params_output_subjs_csv_filepath, index=False)

        return params_df


    def multi_bayesian_testing(self, params_df, dep_vars, test_type="bayesian_estimation"):
        # NOTE: Only correct for non-hierarchical model!!
        # If the model is hierarchical this is incorrect as the independence assumption is violated

        stats_results_str = ""
        for var in dep_vars:
            if test_type == "bayesian_estimation":
                stats_results_var_str = self.bayesian_estimation_test(params_df, dep_var=var)
            elif test_type == "bayes_factor":
                # Not implemented yet
                pass
            stats_results_str += stats_results_var_str + "\n"

        filename = "params_bayesian_hypothesis_tests.txt"
        self.save_results_str(stats_results_str, filename)

    def bayesian_estimation_test(self, params_df, dep_var="v", alpha_ci=0.95):
        # Also knwon as BEST (Bayesian estimation supersedes the t test)
        # http://ski.clps.brown.edu/hddm_docs/howto.html
        # https://www4.stat.ncsu.edu/~reich/ABA/code/ttest
        # https://statmodeling.stat.columbia.edu/2011/04/02/so-called_bayes/
        # http://www.sumsar.net/blog/2014/02/bayesian-first-aid-two-sample-t-test/
        # http://sumsar.net/best_online/
        # https://docs.pymc.io/notebooks/BEST.html
        # http://www2.stat.duke.edu/~rcs46/lecturesModernBayes/601-module3-morebayes/lecture5-more-bayes.pdf
        # https://easystats.github.io/bayestestR/articles/credible_interval.html
        # https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.bayes_mvs.html

        cond_0 = self.conditions[0]
        cond_1 = self.conditions[1]

        # If the 95% Highest Density Interval does not include zero there is a credible difference!
        param_group_0 , param_group_1 = self.model.nodes_db.node[['{}({})'.format(dep_var, cond_0),
                                                                  '{}({})'.format(dep_var, cond_1)]]
        param_group_0 = param_group_0.trace()
        param_group_1 = param_group_1.trace()
        diff_group_means = list(param_group_0 - param_group_1)
        # prob_diff = (diff_group_means != 0).mean()
        diff_credible_interval, _, _ = scipy_stats.bayes_mvs(diff_group_means, alpha=alpha_ci)
        diff_significant = not (diff_credible_interval.minmax[0] <= 0 and diff_credible_interval.minmax[1] >= 0)
        stats_results_str = "Bayesian estimation test for mean of param {} - Group {} mean ({:.3f}) vs group {} mean ({:.3f})" \
                            "- Diff mean: {:.3f}; diff credible interval (alpha {}) ({:.3f}, {:.3f}) includes 0 ? (significant: {})".format(
            dep_var, cond_0, param_group_0.mean(), cond_1, param_group_1.mean(),
            diff_credible_interval.statistic, alpha_ci,  diff_credible_interval.minmax[0],
            diff_credible_interval.minmax[1], diff_significant)

        print(stats_results_str)

        return stats_results_str

    def multi_t_test_in_params(self, params_df, dep_vars):
        # NOTE: Only correct for non-hierarchical model!!
        # If the model is hierarchical this is incorrect as the independence assumption is violated

        stats_results_str = ""
        for var in dep_vars:
            t, p, significant, stats_var_mean = self.t_test_in_params(params_df, dep_var=var, stat="mean")
            t, p, significant, stats_var_std = self.t_test_in_params(params_df, dep_var=var, stat="std")
            stats_results_str += stats_var_mean + "\n" + stats_var_std + "\n"

        filename = "params_t_tests.txt"
        self.save_results_str(stats_results_str, filename)

    def t_test_in_params(self, params_df, dep_var="v", stat="mean", threshold=0.05):
        # NOTE: Only correct for non-hierarchical model!!
        # If the model is hierarchical this is incorrect as the independence assumption is violated

        indices_group_0 = []
        indices_group_1 = []
        params_names = params_df.loc[:, "param"].values
        for i, param in enumerate(params_names):
            if param.startswith(dep_var + "_"):
                num_cond = find_str_between(param, "(", ")")
                if num_cond == "0":
                    indices_group_0.append(i)
                elif num_cond == "1":
                    indices_group_1.append(i)

        data_group_0 = params_df.loc[indices_group_0, stat].astype("float")
        data_group_1 = params_df.loc[indices_group_1, stat].astype("float")
        t_val, p_val = scipy_stats.ttest_rel(data_group_0, data_group_1)
        significant = True if p_val < threshold else False

        stats_results_str = "t-test for param: {} ({}) (group {depends_conditions_by} : 0 (mean:{:.3f}) vs group {depends_conditions_by}:" \
                            " 1 (mean: {:.3f})): t-value={:.3f}, p-val={:.3f} < {} ? (significant: {})".format(
            dep_var, stat, data_group_0.mean(), data_group_1.mean(), t_val,  p_val, threshold, significant,
            depends_conditions_by=self.depends_conditions_by)

        print(stats_results_str)

        return t_val, p_val, significant, stats_results_str


    def save_results_str(self, input_str, filename):
        output_file = os.path.join(self.dir_results_comp_models, filename)
        with open(output_file, "w") as text_file:
            text_file.write(input_str)

def find_str_between(s, first, last):
    try:
        start = s.index( first ) + len( first )
        end = s.index( last, start )
        return s[start:end]
    except ValueError:
        return ""

def corrfunc(x, y, **kws):
    r, _ = scipy_stats.pearsonr(x, y)
    ax = plt.gca()
    ax.annotate("r = {:.2f}".format(r),
                xy=(.1, .9), xycoords=ax.transAxes)

if __name__ == "__main__":
    # THIS SCRIPT IS OUTDATED: See the version of experimental_task_stimulus_habits_one_fmri for the new version

    ddm = RL_HDDM(num_subjs=None, depends_conditions_by="is_overtraining_stim", split_by="stim_id",
                  data_blocks="dev", model_type="RL-HDDM", model_is_hierarchical=False)
    # model = ddm.train_model(num_samples=10, alpha_dual=False, p_outlier=None, check_convergence=False, show_plots=False)
    model = ddm.train_model(num_samples=3000, alpha_dual=False, p_outlier=None, check_convergence=False, show_plots=False)
    # ddm.check_params_collinearity(model)
    ddm.simulate_data(model, num_subjs=None, num_simulations=5, show_plots=True)
    # ddm.simulate_data(model, num_subjs=None, num_simulations=2, show_plots=True)
