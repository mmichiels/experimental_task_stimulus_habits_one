from Experiment import Experiment
import os
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use('TkAgg')

class CompModel(Experiment):
    def __init__(self, psy):
        super(CompModel, self).__init__(psy)
        self.datasets_folder = os.path.join(".", "datasets")

        # S-R-O
        self.states_action_exp_reward = {  # Q-vals table
            0: {
                0: 0,
                1: 0,
            },
            1: {
                0: 0,
                1: 0,
            },
            2: {
                0: 0,
                1: 0,
            },
            3: {
                0: 0,
                1: 0,
            },
        }

    def run(self, in_fMRI=False):
        for i, outcome_dev_idx in enumerate(self.blocks_config_indices):
            self.outcome_dev_idx = outcome_dev_idx
            self.block(i)

    def block(self, num_block):
        for outcome in self.objs.outcomes:  # Reset original points
            outcome.reset_dev(self.psy)

        if self.outcome_dev_idx == -1:  # No devaluation
            trials_stims_indices = self.trials_stims_indices
            self.current_outcome_dev = None
        else:
            trials_stims_indices = self.trials_stims_indices_dev
            outcome_dev = self.objs.outcomes[self.outcome_dev_idx]
            outcome_dev.devaluate(self.psy, self.dev_config["new_points"])
            self.current_outcome_dev = outcome_dev

        points_this_block = 0
        for j, stim_idx in enumerate(trials_stims_indices):  # Each trial must contain at least one trial of each stimuli?
            if len(self.results["num_trial"]) == 0:
                last_num_trial = 0
            else:
                last_num_trial = self.results["num_trial"][-1]
            num_trial = last_num_trial+1
            print("---Trial {}----".format(num_trial))

            if stim_idx != -1:
                self.trial(stim_idx)
            else:
                self.consumption_trial()
            self.results["num_block"].append(num_block)
            self.results["num_trial"].append(num_trial)
            self.results["outcome_dev_id"].append(self.outcome_dev_idx)
            self.results["trial_type"].append(self.trial_type)
            points_this_block += self.results["points_this_trial"][-1]
            self.results["points_this_block"].append(points_this_block)
            self.results["total_time"].append(self.clock_total_time.getTime())

        self.psy.save_results(self.results, format="csv", tmp=True)


    def trial(self, stim_idx):
        self.trial_type = 0
        outcome_idx = -1
        points_earned = 0
        response_accuracy = 0

        response_idx, outcome_idx, response_accuracy, points_earned, outcome_not_selected_idx, max_poss_points = self.response(stim_idx)

        # Update results
        response_time = 0
        in_time_code = 0
        self.update_results(stim_idx, response_idx, outcome_idx, outcome_not_selected_idx, response_time,
                            in_time_code, points_earned, response_accuracy, max_poss_points)

    def response(self, stim_idx):
        action_idx = self.action_selection_policy(stim_idx)
        outcome_idx, response_accuracy, reward, outcome_not_selected_id, max_possible_points = self.execute_action(stim_idx, action_idx)
        self.update_q_val(stim_idx, action_idx, reward)

        return action_idx, outcome_idx, response_accuracy, reward, outcome_not_selected_id, max_possible_points

    def execute_action(self, stim_idx, response_idx):
        ok_key = True
        in_time_code = 0

        if ok_key and in_time_code == 0:  # Response in time
            ok_response = True
            outcome_idx = self.s_r_o_hashmap[stim_idx][response_idx]
            points_earned, response_accuracy, outcome_not_selected_id, max_possible_points = self.calculate_points_and_accuracy(ok_response,
                                                                                  outcome_idx, stim_idx=stim_idx)


        print("Stim: {}. Response: {}. Points earned: {}. Max possible points: {}".format(
            stim_idx, response_idx, points_earned, max_possible_points
        ))

        return outcome_idx, response_accuracy, points_earned, outcome_not_selected_id, max_possible_points


def softmax(x, temp=3, use_temp=True):
    if use_temp:  # Use temperature to allow to change the exploration/exploitation balance
        probs = np.exp(x/temp) / sum(np.exp(x/temp))
    else:
        probs = np.exp(x) / sum(np.exp(x))

    return probs