from utils_psychopy import *
import os

def show_instructions(psy, control_keys, preload_imgs=True):
    imgs_filenames = os.listdir(psy.dir_instructions_imgs)
    total_imgs = 9  #len(imgs_filenames)

    if preload_imgs:
        imgs = []
        for i in range(1, total_imgs+1):
            img = load_img(psy, i)
            imgs.append(img)

    start_img_index = 1
    curr_img_index = start_img_index

    while True:
        if preload_imgs:
            imgs[curr_img_index - 1].draw()
            psy.win.flip()
        else:
            load_and_show_img(psy, curr_img_index)

        # keys_pressed = event.getKeys()
        keys_pressed = psychopy.event.waitKeys()

        if len(keys_pressed) == 1:
            this_key = keys_pressed[0]
            if this_key == control_keys["forward_key"] and curr_img_index < total_imgs:  # Not the last image
                curr_img_index += 1
            elif this_key == control_keys["backward_key"] and curr_img_index > start_img_index:  # Not the 1st image
                curr_img_index -= 1
            elif this_key == control_keys["exit_key"] and curr_img_index==total_imgs:  # Last image
                break

        psychopy.event.clearEvents()

    # print("Instructions done!")

def load_and_show_img(psy, curr_img_index):
    img = load_img(psy, curr_img_index)

    img.draw()
    psy.win.flip()

def load_img(psy, curr_img_index):
    img_filepath = os.path.join(psy.dir_instructions_imgs, "instr{}.JPG".format(curr_img_index))

    img = psychopy.visual.ImageStim(
        win=psy.win,
        name='image',
        image=img_filepath, mask=None,
        ori=0, pos=(0, 0), size=(1.33, 1),
        interpolate=True)

    return img