from utils_psychopy import *
import numpy as np
import itertools
import math
import psychopy
from psychopy import visual

class GameObjs():
    def __init__(self, psy, participant_number):
        self.psy = psy
        self.participant_number = participant_number

        max_points = 100
        min_points = 5
        self.first_o_points = [max_points, max_points, min_points, 0]
        self.o_max_points_indices = []
        self.o_min_points_indices = []
        for i, points in enumerate(self.first_o_points):
            if points == max_points:
                self.o_max_points_indices.append(i)
            elif points == min_points:
                self.o_min_points_indices.append(i)

        self.first_o_names = ["Diamante", "Oro", "Moneda"]
        self.optimal_points_info = {"text": "¡Ummm! A este alien le encantó esa galleta.", "color": "white"}
        self.suboptimal_points_info = {"text": "¡Puagh! A este alien no le gustó esa galleta.", "color": "red"}
        self.normal_text_size = 0.044
        self.small_text_size = 0.03

        self.stims_def_pos = (0, 0)
        self.stims_def_size = (0.19, 0.19)
        self.responses_pos = [(-0.5, 0), (0.5, 0)]
        self.responses_def_size = (1.0, 0.7)
        self.outcomes_def_pos = (0, 0)
        self.outcomes_def_size = (0.16, 0.16)
        self.outcomes_first_sizes = [(0.16, 0.16), (0.163, 0.163), (0.14, 0.14)]
        self.outcomes_pos_consumption_trial = [(-0.3, 0), (0.3, 0)]

        # self.stims_def_pos = (0, 0)
        # self.stims_def_size = (0.18, 0.18)
        # self.responses_pos = [(-0.5, 0), (0.5, 0)]
        # self.responses_def_size = (0.9, 0.6)
        # self.outcomes_def_pos = (0, 0)
        # self.outcomes_def_size = (0.15, 0.15)
        # self.outcomes_first_sizes = [(0.15, 0.15), (0.153, 0.153), (0.13, 0.13)]
        # self.outcomes_pos_consumption_trial = [(-0.3, 0), (0.3, 0)]

        self.load_game_objs()

    def load_game_objs(self):
        self.total_stims = 0
        self.total_responses = 0
        self.total_outcomes = 0
        stim_init_str = "stim_"
        response_init_str = "response_"
        outcome_init_str = "outcome_"
        img_extension = ".png"

        for img_filename in os.listdir(self.psy.dir_game_imgs):
            if img_filename.startswith(stim_init_str):
                self.total_stims += 1
                if self.total_stims == 1:
                    img_name, img_extension = os.path.splitext(img_filename)
            elif img_filename.startswith(response_init_str):
                self.total_responses += 1
            elif img_filename.startswith(outcome_init_str):
                self.total_outcomes += 1

        s_comb, r_comb, o_comb = self.counterbalancing_s_r_o()

        self.stims = self.create_stims(s_comb, img_extension, stim_init_str)
        self.responses = self.create_responses(r_comb, img_extension, response_init_str)
        self.outcomes = self.create_outcomes(o_comb, img_extension, outcome_init_str)

        if not self.psy.is_comp_model:
            self.fixation_stim = visual.Circle(win=self.psy.win, radius=0.01, color="red")
            # name='fixation',
            #                                               text='.',
            #                                               font='Arial',
            #                                               units='height', pos=(0, 0), height=0.4, wrapWidth=None, ori=0,
            #                                               color='red', colorSpace='rgb', opacity=1,
            #                                               languageStyle='LTR',
            #                                               depth=0.0)


            self.waiting_fmri_text = visual.TextStim(win=self.psy.win, name='waiting_fmri',
                                                                    text='Esperando resonancia...',
                                                                    font='Arial',
                                                                    units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                                    color='white', colorSpace='rgb', opacity=1,
                                                                    languageStyle='LTR',
                                                                    depth=0.0)

            self.instr_consumption_trial = visual.TextStim(win=self.psy.win, name='instr_consumption_trial',
                                                                    text='Los aliens están distraídos...',
                                                                    font='Arial',
                                                                    units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                                    color='white', colorSpace='rgb', opacity=1,
                                                                    languageStyle='LTR',
                                                                    depth=0.0)

            arrow_stim_img_filepath = os.path.join(self.psy.dir_game_imgs, "arrows" + img_extension)
            self.arrows_stim = self.psy.load_img_from_disk(arrow_stim_img_filepath, size=(0.2, 0.2))

            self.outcome_points_unknown = visual.TextStim(win=self.psy.win, name='outcome_points_unknown',
                                                                   text='??',
                                                                   font='Arial',
                                                                   units='height', pos=(0, -0.1), height=0.04, wrapWidth=None, ori=0,
                                                                   color='white', colorSpace='rgb', opacity=1,
                                                                   languageStyle='LTR',
                                                                   depth=0.0)

            self.outcome_text_slow = visual.TextStim(win=self.psy.win, name='outcome_text_slow',
                                                              text='¡Muy lento! Por favor, responde más rápido.',
                                                              font='Arial',
                                                              units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                              color='white', colorSpace='rgb', opacity=1,
                                                              languageStyle='LTR',
                                                              depth=0.0)

            self.outcome_text_fast = visual.TextStim(win=self.psy.win, name='outcome_text_fast',
                                                              text='¡MUY RÁPIDO! ¡Te quedas sin recompensa!',
                                                              font='Arial',
                                                              units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                              color='white', colorSpace='rgb', opacity=1,
                                                              languageStyle='LTR',
                                                              depth=0.0)

            text_intro_new_block = ""  # Va a empezar un nuevo bloque. \n\n"
            self.instr_block_no_dev = visual.TextStim(win=self.psy.win, name='instr_block_no_dev',
                                                               text=text_intro_new_block + 'No hay interferencias galácticas. '
                                                                                           '\n Todas las recompensas tienen su valor original: \n'
                                                                                           'Diamante y oro: 100 puntos cada uno. \n'
                                                                                           'Moneda: 5 puntos cada una \n\n '
                                                                                           'No te fijes solo en qué galletas '
                                                                                           'le gustan a cada alien: Intenta RECORDAR'
                                                                                           ' qué GALLETA te da ORO y qué galleta te da'
                                                                                           ' DIAMANTE',
                                                               font='Arial',
                                                               units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                               color='white', colorSpace='rgb', opacity=1,
                                                               languageStyle='LTR',
                                                               depth=0.0)

            self.instr_block_dev_title = visual.TextStim(win=self.psy.win, name='instr_block_dev_title',
                                                                  text=text_intro_new_block + '¡¡Hay interferencias galácticas!!',
                                                                  font='Arial',
                                                                  units='height', pos=(0, 0.3), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                                  color='white', colorSpace='rgb', opacity=1,
                                                                  languageStyle='LTR',
                                                                  depth=0.0)

            self.instr_block_dev = visual.TextStim(win=self.psy.win, name='instr_block_dev',
                                                            text='',
                                                            font='Arial',
                                                            units='height', pos=(0, -0.1), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                            color='white', colorSpace='rgb', opacity=1,
                                                            languageStyle='LTR',
                                                            depth=0.0)

            self.text_points_obj_optimal = visual.TextStim(win=self.psy.win, name='text_points_obj_optimal',
                                                            text=self.optimal_points_info["text"],
                                                            font='Arial',
                                                            units='height', pos=(0, -0.15), height=self.normal_text_size, wrapWidth=None,
                                                            ori=0,
                                                            color=self.optimal_points_info["color"], colorSpace='rgb', opacity=1,
                                                            languageStyle='LTR',
                                                            depth=0.0)

            self.text_points_obj_suboptimal = visual.TextStim(win=self.psy.win, name='text_points_obj_suboptimal',
                                                            text=self.suboptimal_points_info["text"],
                                                            font='Arial',
                                                            units='height', pos=(0, -0.15), height=self.small_text_size, wrapWidth=None,
                                                            ori=0,
                                                            color=self.suboptimal_points_info["color"], colorSpace='rgb', opacity=1,
                                                            languageStyle='LTR',
                                                            depth=0.0)

            self.results_block_info = visual.TextStim(win=self.psy.win, name='results_block_info',
                                                               text='',
                                                               font='Arial',
                                                               units='height', pos=(0, 0), height=self.small_text_size, wrapWidth=None, ori=0,
                                                               color='white', colorSpace='rgb', opacity=1,
                                                               languageStyle='LTR',
                                                               depth=0.0)

            self.results_block_end_info = visual.TextStim(win=self.psy.win, name='results_block_end_info',
                                                                   text='¡Bloque terminado!',
                                                                   font='Arial',
                                                                   units='height', pos=(0, 0.2), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                                   color='white', colorSpace='rgb', opacity=1,
                                                                   languageStyle='LTR',
                                                                   depth=0.0)

            self.results_block_dev_info = visual.TextStim(win=self.psy.win, name='results_block_dev_info',
                                                                   text='' ,
                                                                   font='Arial',
                                                                   units='height', pos=(0, -0.1), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                                   color='white', colorSpace='rgb', opacity=1,
                                                                   languageStyle='LTR',
                                                                   depth=0.0)

            self.countdown_info = visual.TextStim(win=self.psy.win, name='instr_consumption_trial',
                                                           text='',
                                                           font='Arial',
                                                           units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                           color='white', colorSpace='rgb', opacity=1,
                                                           languageStyle='LTR',
                                                           depth=0.0)

    def draw_end_game_info(self, total_points, in_fMRI):
        self.end_game_info = visual.TextStim(win=self.psy.win, name='end_game_info',
                                                      text='FIN DEL JUEGO \n\n Puntos totales: {} \n\n '
                                                           'Gracias por participar'.format(total_points),
                                                      font='Arial',
                                                      units='height', pos=(0, 0), height=self.normal_text_size, wrapWidth=None, ori=0,
                                                      color='white', colorSpace='rgb', opacity=1,
                                                      languageStyle='LTR',
                                                      depth=0.0)
        if not in_fMRI:
            self.end_game_info.text += " \n\n Por favor, ahora lee el PDF con las instrucciones " \
                                       "para enviar tus resultados."

        self.end_game_info.draw()

    def counterbalancing_s_r_o(self, randomize_outcomes=False):
        stims_indices = list(range(self.total_stims))
        responses_indices = list(range(self.total_responses))
        outcomes_indices = list(range(self.total_outcomes))

        all_s_r_o_combinations = [stims_indices, responses_indices, outcomes_indices]
        all_s_r_o_combinations = list(itertools.product(*all_s_r_o_combinations))

        s_combinations = shift_list_n_times(stims_indices)
        r_combinations = shift_list_n_times(responses_indices)
        o_combinations = shift_list_n_times(outcomes_indices)

        participant_number = self.participant_number
        if participant_number > len(all_s_r_o_combinations) - 1:
            participant_number = participant_number - \
                                     math.trunc((participant_number / len(all_s_r_o_combinations))) * len(
                all_s_r_o_combinations)

        s_r_o_comb = all_s_r_o_combinations[participant_number]
        s_comb = s_combinations[s_r_o_comb[0]]
        r_comb = r_combinations[s_r_o_comb[1]]

        if randomize_outcomes:
            o_comb = o_combinations[s_r_o_comb[2]]
        else:
            o_comb = list(range(self.total_outcomes))

        return s_comb, r_comb, o_comb

    def create_stims(self, order_objs, img_extension, init_str):
        stims = []

        for i, idx_img in enumerate(order_objs):
            img = self.load_img(idx_img, img_extension, init_str, self.stims_def_pos, self.stims_def_size)
            stim = Stim(self.psy, img)
            stims.append(stim)

        return stims

    def create_responses(self, order_objs, img_extension, init_str):
        responses = []

        for i, idx_img in enumerate(order_objs):
            img = self.load_img(idx_img, img_extension, init_str, self.responses_pos[i], self.responses_def_size)
            response = Response(self.psy, img)
            responses.append(response)

        return responses

    def create_outcomes(self, order_objs, img_extension, init_str):
        outcomes = []

        for i, idx_img in enumerate(order_objs):
            points = self.first_o_points[i] if i < len(self.first_o_points) else 0
            name = self.first_o_names[idx_img] if idx_img < len(self.first_o_names) else None
            size = self.outcomes_first_sizes[idx_img] if idx_img < len(self.outcomes_first_sizes) else self.outcomes_def_size

            img = self.load_img(idx_img, img_extension, init_str, self.outcomes_def_pos, size)
            outcome = Outcome(self.psy, img, name, points)
            outcomes.append(outcome)

        return outcomes

    def outcomes_change_pos_consumption(self, outcome_inds):
        for i, outcome_idx in enumerate(outcome_inds):
            pos = self.outcomes_pos_consumption_trial[i] if i < len(self.outcomes_pos_consumption_trial) else (0, 0)
            self.outcomes[outcome_idx].img.pos = pos
            self.outcomes[outcome_idx].img.draw()

        return 0

    def restore_outcomes_pos(self, outcome_inds):
        for i, outcome_idx in enumerate(outcome_inds):
            self.outcomes[outcome_idx].img.pos = self.outcomes_def_pos

    def show_text_points_info(self, accuracy):
        if accuracy == 0:
            self.text_points_obj_suboptimal.draw()
        elif accuracy == 1:
            self.text_points_obj_optimal.draw()

    def load_img(self, img_idx, img_extension, init_str, pos=None, size=None):
        new_pos = (0, 0) if pos is None else pos
        new_size = (0, 0) if size is None else size

        img_name = os.path.join(self.psy.dir_game_imgs, init_str + str(img_idx + 1) + img_extension)
        img = self.psy.load_img_from_disk(img_name, new_pos, new_size)

        return img

class GameObj():
    def __init__(self, psy, img):
        self.img = img

class Stim(GameObj):
    def __init__(self, psy, img):
        super(Stim, self).__init__(psy, img)

class Response(GameObj):
    def __init__(self, psy, img):
        super(Response, self).__init__(psy, img)

class Outcome(GameObj):
    def __init__(self, psy, img, name, points=0, is_dev=False):
        super(Outcome, self).__init__(psy, img)

        self.name = name
        self.original_points = points
        self.points = points
        self.is_dev = is_dev
        self.create_points_info(psy)

    def create_points_info(self, psy):
        if not psy.is_comp_model:
            self.points_obj = visual.TextStim(win=psy.win, name='outcome_points',
                                                       text='+ {}'.format(self.points),
                                                       font='Arial',
                                                       units='height', pos=(0, -0.1), height=0.04, wrapWidth=None, ori=0,
                                                       color='white', colorSpace='rgb', opacity=1,
                                                       languageStyle='LTR',
                                                       depth=0.0)

    def devaluate(self, psy, new_points):
        self.points = new_points
        self.is_dev = True
        self.create_points_info(psy)

    def reset_dev(self, psy):
        self.points = self.original_points
        self.is_dev = False
        self.create_points_info(psy)

