
import utils_psychopy as utils_psy
import instructions as instr
import Experiment as exp
import computational_models.RW as comp_models_rw

debug = False
comp_model = False
instructions = True
in_fMRI = True
session_number = 3
comp_model_name = "RW"

if __name__ == "__main__":
    psy_obj = utils_psy.Psychopy(debug, session_number, comp_model, in_fMRI)

    if comp_model:
        experiment = comp_models_rw.RW(psy_obj)
    else:
        experiment = exp.Experiment(psy_obj)
        if instructions:
            instr.show_instructions(psy_obj, experiment.instruction_keys)

    experiment.run()

    psy_obj.save_results(experiment.results, format="csv")