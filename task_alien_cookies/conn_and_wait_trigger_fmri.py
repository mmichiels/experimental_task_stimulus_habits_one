
import serial
import serial.tools.list_ports
import time
from psychopy import sound, gui, visual, core, data, event, logging, clock

MRI_DEVICE_ID = 'COM6'  # Windows
# MRI_DEVICE_ID = '/dev/pts/6'  # Linux/MacOS

def conn_and_wait_for_MRI_trigger_dsr(baudrate=9600):
    # THIS is the correct method for Siemens MRI machines. Tested only with Siemens: syngo MR E11.

    # DSR (DatasetReady) pin is True when the MRI is not ready. It turned False when the MRI becomes ready
    # and sends the trigger by turning the DSR pin False.
    # WARNING: Reading the DSR pin in Linux FAILS.
    # (In Windows it works properly). It is a known bug: https://bugs.launchpad.net/ubuntu/+source/pyserial/+bug/304469

    print("Trying to connect to MRI with DSR device_id: {} ...".format(MRI_DEVICE_ID))

    clock_total_time = core.Clock()
    ser = serial.Serial(MRI_DEVICE_ID, baudrate=baudrate)  # open first serial port

    device_ready = False
    prev_dsr = ser.dsr
    while not device_ready:
        ser.close()
        ser.open()
        curr_dsr = ser.dsr
        # print("dsr: ", dsr)

        if curr_dsr != prev_dsr:
            print("======= MRI trigger received (DatasetReady signal turned False). Device ready. Clock reset =======")
            device_ready = True
            clock_total_time = core.Clock()  # Start timing response

    ser.close()
    print("-- MRI conn done --")

    return clock_total_time


def conn_and_wait_for_MRI_trigger_string(baudrate=9600):
    print("Trying to connect to MRI with in_waiting device_id: {} ...".format(MRI_DEVICE_ID))

    clock_total_time = core.Clock()
    ser = serial.Serial(MRI_DEVICE_ID, baudrate=baudrate)  # open first serial port
    #time.sleep(2)  ### wait for 2 sec to ensure scanner is ready; CNI wiki default 0.1

    if not ser.dtr:
        print("======= MRI was already ready (DatasetReady signal). Clock reset =======")
    else:
        device_ready = False
        while not device_ready:
            #time.sleep(0.5)
            #print("...Waiting for MRI trigger (DatasetReady signal)...")
            chars_received = ser.in_waiting

            if chars_received > 0:
                text_received = str(ser.read(chars_received))
                print("======= MRI trigger received: {} (characters received: {}) (DatasetReady signal). Device ready."
                      " Clock reset =======".format(text_received, chars_received))
                device_ready = True
                clock_total_time = core.Clock()  # Start timing response

    print("-- MRI conn done --")

    return clock_total_time

def send_trigger(ser, text):
    ser.write(b"\n")

if __name__ == "__main__":
    # To test this with a virtual serial port, just run in the Linux terminal:
    # socat -d -d pty,raw,echo=0 pty,raw,echo=0
    # And from another terminal:
    # echo "testing..." > /dev/pts/20

    conn_and_wait_for_MRI_trigger_dsr()
