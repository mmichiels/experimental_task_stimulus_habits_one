from utils_psychopy import *
import conn_and_wait_trigger_fmri as conn_fmri
import numpy as np
import itertools
import math
import random
import game_objs
import events

class Experiment:
    def __init__(self, psy):
        self.psy = psy
        self.session_number = int(psy.exp_info["session_number"])
        self.participant_number = int(psy.exp_info["participant_number"])
        print("PARTICIPANT NUMBER: ", self.participant_number)
        self.in_fMRI = psy.exp_info['in_fMRI']
        self.objs = game_objs.GameObjs(self.psy, self.participant_number)  # Psychopy objects (stimuli, responses, etc)
        self.clock_total_time = core.Clock()
        self.clock_onsets = core.Clock()

        # Block-trials config (every block has num_trials_per_block; Total trials = num_blocks*num_trials_per_block)

        # Controls
        if self.in_fMRI:
            self.instruction_keys = {
                "backward_key": "num_1",
                "forward_key": "num_2",
                "exit_key": "escape"
            }
            self.left_decision_key = "num_1"
            self.right_decision_key = "num_2"
            self.control_keys = [self.left_decision_key, self.right_decision_key]
            self.next_screen_key = "space"
            self.press_space_text = ""
        else:
            self.instruction_keys = {
                "backward_key": "left",
                "forward_key": "right",
                "exit_key": "escape"
            }
            self.left_decision_key = "q"
            self.right_decision_key = "p"
            self.control_keys = [self.left_decision_key, self.right_decision_key]
            self.next_screen_key = "space"
            self.press_space_text = "Presiona espacio para continuar."

        if self.session_number == 0:
            self.num_trials_per_block = 4  # 44 Minimum recommended: 9 because it allows
            self.consumption_trial_every = 2
            self.num_trials_per_block_dev = 4

            self.training_per_over_stim = 0.5
            self.training_times_over_time = 3.5
            self.training_consumption_trials = True
            self.dev_per_over_stim = 0.5
            self.dev_times_over_time = 1
            self.dev_consumption_trials = True

            self.blocks_design = [
                "training",
                "dev",  # Mini-block as a test to warn participants to remember the gold and diamond positions
                "training",
            ]

            # Duration screens:
            self.duration_fixation = 3.5
            self.duration_fixation_consumption_trial = np.random.uniform(0.2, 0.3)
            self.duration_instr_consumption_trial = 1.5
            self.duration_instr_no_dev = 2.0
            self.duration_till_show_responses_imgs = 0.25  # 0.2
            self.duration_between_response_outcome = 3  # + np.random.uniform(0, self.duration_max_jitter)
            self.duration_between_response_outcome_consumption_trial = 0.3  # + np.random.uniform(0, self.duration_max_jitter)
            self.duration_outcome_presentation = 1  # 1.4
            self.duration_iti_before_trial = 3  # + np.random.uniform(0, self.duration_max_jitter)
            self.duration_iti_before_consumption_trial = 3
            self.duration_iti_before_trial_dev = self.duration_iti_before_trial
            self.duration_iti_before_trial_dev_consumption_trial = self.duration_iti_before_trial
            self.duration_end_game_screen = 10
            # Total duration between trials must be 5 for training blocks and 6 in dev blocks, to allow clean signals in
            # fMRI
            self.duration_total_between_trials = self.duration_fixation + self.duration_till_show_responses_imgs + \
                                                 self.duration_between_response_outcome + \
                                                 self.duration_outcome_presentation + self.duration_iti_before_trial
            self.duration_max_jitter = 1
        elif self.session_number <= 2:
            self.num_trials_per_block = 44  # 44 Minimum recommended: 9 because it allows
            # stims_overtraining to appear 3.5 times more than stims_undertraining. Num_trials less than 22 can
            # cause num_trials to be a little different than the input number here, to try to adjust the number of stims
            # per trial according to the calculate_trials_stims_indices function
            self.consumption_trial_every = 13
            self.num_trials_per_block_dev = 4

            self.training_per_over_stim = 0.5
            self.training_times_over_time = 3.5
            self.training_consumption_trials = True
            self.dev_per_over_stim = 0.5
            self.dev_times_over_time = 1
            self.dev_consumption_trials = True

            self.blocks_design = [
                "training",
                "training",
                "training",
                "dev",  # Mini-block as a test to warn participants to remember the gold and diamond positions
                "training",
            ]

            # Duration screens:
            self.duration_fixation = 0.25
            self.duration_fixation_consumption_trial = 0.25
            self.duration_instr_consumption_trial = 1.5
            self.duration_instr_no_dev = 2.0
            self.duration_till_show_responses_imgs = 0.25  # 0.2
            self.duration_between_response_outcome = 0.3  # 2
            self.duration_between_response_outcome_consumption_trial = 0.3  # + np.random.uniform(0, self.duration_max_jitter)
            self.duration_outcome_presentation = 1  # 1.4
            self.duration_iti_before_trial = 3  # ISI (inter-stimulus interval)
            self.duration_iti_before_consumption_trial = 3  # ISI (inter-stimulus interval)
            self.duration_iti_before_trial_dev = self.duration_iti_before_trial
            self.duration_iti_before_trial_dev_consumption_trial = self.duration_iti_before_trial
            self.duration_end_game_screen = 10
            # Total duration between trials must be 5 for training blocks and 6 in dev blocks, to allow clean signals in
            # fMRI
            self.duration_total_between_trials = self.duration_fixation + self.duration_till_show_responses_imgs + \
                                                 self.duration_between_response_outcome + \
                                                 self.duration_outcome_presentation + self.duration_iti_before_trial
            self.duration_max_jitter = 0
        elif self.session_number == 3:
            self.num_trials_per_block = 44
            self.consumption_trial_every = 13
            self.num_trials_per_block_dev_mult = 1  # 2.72  # X times more than num_trials_per_block
            self.num_trials_per_block_dev = int(self.num_trials_per_block * self.num_trials_per_block_dev_mult)  # X% more than normal blocks  # 120
            self.training_per_over_stim = 0.5
            self.training_times_over_time = 3.5
            self.training_consumption_trials = True
            self.dev_per_over_stim = 0.5
            self.dev_times_over_time = 1
            self.dev_consumption_trials = True

            self.blocks_design = [
                "training",
                "training",
                "dev",
                "training",
                "dev"
            ]

            # Duration screens:
            self.duration_fixation = 3.5
            self.duration_fixation_consumption_trial = 0.25
            self.duration_instr_consumption_trial = 1.5
            self.duration_instr_no_dev = 2.0
            self.duration_till_show_responses_imgs = 0.25  # 0.2
            self.duration_between_response_outcome = 3  # + np.random.uniform(0, self.duration_max_jitter)
            self.duration_between_response_outcome_consumption_trial = 0.3  # + np.random.uniform(0, self.duration_max_jitter)
            self.duration_outcome_presentation = 1  # 1.4
            self.duration_iti_before_trial = 3  # + np.random.uniform(0, self.duration_max_jitter)
            self.duration_iti_before_consumption_trial = 3
            self.duration_iti_before_trial_dev = self.duration_iti_before_trial
            self.duration_iti_before_trial_dev_consumption_trial = self.duration_iti_before_trial
            self.duration_end_game_screen = 10
            # Total duration between trials must be 5 for training blocks and 6 in dev blocks, to allow clean signals in
            # fMRI
            self.duration_max_jitter = 1  # np.random.uniform(0, 1)

        self.num_blocks = len(self.blocks_design)
        self.max_consecutive_stims = 2
        self.num_overtraining_stim, self.num_undertraining_stim = self.overtraining_stims_stats(self.training_per_over_stim)
        self.stim_overtraining_indices = list(range(self.num_overtraining_stim))
        self.stim_undertraining_indices = list(range(self.num_overtraining_stim, self.num_undertraining_stim))
        self.blocks_config_indices = self.calculate_blocks_config_indices()

        # Trial types
        self.trial_type_config = {
            0: "trial",
            1: "consumption_trial",
        }
        self.trial_type = 0

        # Outcomes config
        self.s_r_o_hashmap = {  # Design table: Stimulus id -> Response id -> outcome id
            0: {  # S0 (overtrained)
                0: 0,  # R0 -> O(100)
                1: 2,  # R1 -> O(5)
            },
            1: {  # S1 (overtrained)
                0: 2,  # R0 -> O(5)
                1: 1,  # R1 -> O(100)
            },
            2: {  # S0 (undertrained)
                0: 1,  # R0 -> O(100)
                1: 2,  # R1 -> O(5)
            },
            3: {  # S1 (undertrained)
                0: 2,  # R0 -> O(5)
                1: 0,  # R1 -> O(100)
            },
        }

        # Devaluation config (for an entire block)
        self.total_dev_levels = len(self.objs.o_max_points_indices) + 1  # +1 to include the no dev option
        self.dev_config = {
            "new_points": 0,
        }

        self.current_outcome_dev = None
        self.outcome_dev_idx = -1  # No devaluation

        # Response time pressure
        self.min_response_time = 0
        self.max_response_time_low = 0.6
        self.response_in_time_map = {
            0: "in time",
            1: "too fast",
            2: "too_slow"
        }
        self.time_pressure_hashmap = {
            0: self.max_response_time_low,
        }
        self.time_pressure_level = int(psy.exp_info["time_pressure"])
        self.max_response_time = self.time_pressure_hashmap[self.time_pressure_level]
        self.max_response_consumption_trial = 4

        # Participant results
        self.results = {
            "num_block": [],
            "num_trial": [],
            "trial_type": [],
            "stim_id": [],
            "is_overtraining_stim": [],
            "response_id": [],
            "response_accuracy": [],
            "response_time": [],
            "response_in_time": [],
            "points_this_trial": [],
            "points_max_possible_this_trial": [],
            "points_this_block": [],
            "outcome_selected_id": [],
            "outcome_not_selected_id": [],
            "outcome_dev_id": [],
            "outcome_dev_selected": [],
            "response_switch": [],
            "onset_iti": [],
            "onset_fixation": [],
            "onset_stimulus": [],
            "onset_response_window": [],
            "onset_between_response_outcome": [],
            "onset_outcome_presentation": [],
            "onset_end_trial": [],
            "total_time_end_trial": [],
            "total_points": [],
        }

    def run(self):
        for i, outcome_dev_idx in enumerate(self.blocks_config_indices):
            self.outcome_dev_idx = outcome_dev_idx
            self.block(i)

        self.objs.draw_end_game_info(self.results["total_points"][-1], in_fMRI=self.in_fMRI)
        self.psy.win.flip()
        psychopy.core.wait(self.duration_end_game_screen)

    def block(self, num_block):
        for outcome in self.objs.outcomes:  # Reset original points
            outcome.reset_dev(self.psy)

        if self.outcome_dev_idx == -1:  # No devaluation
            self.trials_stims_indices = self.calculate_trials_stims_indices(self.num_trials_per_block,
                                                                            self.training_per_over_stim,
                                                                            self.training_times_over_time ,
                                                                            self.training_consumption_trials,
                                                                            self.outcome_dev_idx)
            trials_stims_indices = self.trials_stims_indices
            self.current_outcome_dev = None
            self.objs.instr_block_no_dev.text = 'No hay interferencias galácticas. \n' \
                                                ' Todas las recompensas tienen su valor original: \n' \
                                                ' Diamante y oro: 100 puntos cada uno. \n' \
                                                ' Moneda: 5 puntos cada una \n\n' \
                                                ' No te fijes solo en qué galletas le gustan a cada alien: ' \
                                                'Intenta RECORDAR qué GALLETA te da ORO y qué galleta te ' \
                                                'da DIAMANTES. \n\n' \
                                                ' {}'.format(self.press_space_text)
            self.objs.instr_block_no_dev.draw()
            self.psy.win.flip()
            psychopy.core.wait(self.duration_instr_no_dev)
        else:
            self.trials_stims_indices_dev = self.calculate_trials_stims_indices(self.num_trials_per_block_dev,
                                                                                self.dev_per_over_stim,
                                                                                self.dev_times_over_time,
                                                                                self.dev_consumption_trials,
                                                                                self.outcome_dev_idx)
            trials_stims_indices = self.trials_stims_indices_dev
            outcome_dev = self.objs.outcomes[self.outcome_dev_idx]
            outcome_dev.devaluate(self.psy, self.dev_config["new_points"])
            self.objs.instr_block_dev.text = "Las recompensas de {} (antes {} puntos) pasan a valer {} puntos. " \
                                             "\n\n\n\n\n ¡¡Evita coger estas recompensas e intenta ganar las monedas en su lugar!! " \
                                             "\n\n El resto de recompensas tienen su valor original." \
                                             "\n\n Verás '??' en lugar del valor de las recompensas. " \
                                             "\n\n {}".format(
                outcome_dev.name, outcome_dev.original_points, outcome_dev.points, self.press_space_text)
            outcome_dev.img.draw()
            self.current_outcome_dev = outcome_dev
            self.objs.instr_block_dev_title.draw()
            self.objs.instr_block_dev.draw()

            self.psy.win.flip()

        points_this_block = 0
        events.get_response_wait([self.next_screen_key])

        if self.in_fMRI:
            events.get_response_wait([self.next_screen_key])  # Second confirmation key press
            self.objs.waiting_fmri_text.draw()
            self.psy.win.flip()
            new_clock_total_time = conn_fmri.conn_and_wait_for_MRI_trigger_dsr()
            if num_block == 0:
                self.clock_total_time = new_clock_total_time
                print("TIME reset: ", self.clock_total_time.getTime())

        for j, stim_idx in enumerate(trials_stims_indices):  # Each trial must contain at least one trial of each stimuli?
            if len(self.results["num_trial"]) == 0:
                last_num_trial = 0
            else:
                last_num_trial = self.results["num_trial"][-1]
            num_trial = last_num_trial+1

            if stim_idx != -1:
                self.trial(stim_idx, num_trial)
            else:
                self.consumption_trial(num_trial)

            self.results["num_block"].append(num_block)
            self.results["num_trial"].append(num_trial)
            self.results["outcome_dev_id"].append(self.outcome_dev_idx)
            self.results["trial_type"].append(self.trial_type)
            points_this_block += self.results["points_this_trial"][-1]
            self.results["points_this_block"].append(points_this_block)
            self.results["total_time_end_trial"].append(self.clock_total_time.getTime())
            self.psy.save_results(self.results, format="csv", tmp=True)

        # Show temp results to the user
        self.objs.results_block_info.text = "¡Bloque {} de {} terminado! \n\n Puntos en este bloque: {} " \
                                            "\n Puntos totales: {} \n {}". \
            format(num_block+1, self.num_blocks, self.results["points_this_block"][-1], self.results["total_points"][-1], self.press_space_text)
        self.objs.results_block_info.draw()
        self.psy.win.flip()

        if self.outcome_dev_idx != -1:
            self.objs.results_block_dev_info.text = '\n\n\n\n ¡Recuerda que las recompensas de {} ' \
                                                    'valían 0 puntos! \n\n Las demás recompensas mantenían su valor.' \
                                                    '\n\n {}'.format(self.current_outcome_dev.name, self.press_space_text)
            self.objs.results_block_end_info.draw()
            self.objs.results_block_dev_info.draw()
            self.current_outcome_dev.img.draw()
            self.psy.win.flip()

        self.psy.save_results(self.results, format="csv", tmp=True)
        events.get_response_wait([self.next_screen_key])
        logging.flush()

    def trial(self, stim_idx, num_trial, show_countdown_text=False):
        self.trial_type = 0
        onsets = {}

        if num_trial == 1:
            self.clock_onsets.reset()

        onsets["iti"] = self.wait_iti(num_trial, show_countdown_text)

        # Fixation
        self.objs.fixation_stim.draw()
        onsets["fixation"] = self.clock_onsets.getTime()
        self.psy.win.flip()
        psychopy.core.wait(self.duration_fixation)
        psychopy.event.clearEvents()

        # Stimulus imgs
        # stim_idx = np.random.randint(0, self.objs.total_stims)
        self.objs.stims[stim_idx].img.autoDraw = True
        onsets["stimulus"] = self.clock_onsets.getTime()
        self.psy.win.flip()

        # Delay between stim and response imgs
        ok_response, response_idx, response_time = events.get_response(self.control_keys, True, self.duration_till_show_responses_imgs)
        in_time_code = events.check_response_in_time(response_time, self.duration_till_show_responses_imgs, self.max_response_time)

        outcome_idx = -1
        points_earned = 0
        response_accuracy = 0
        outcome_not_selected_idx = -1
        max_poss_points = 0
        onsets["response_window"] = self.clock_onsets.getTime()
        onsets["between_response_outcome"] = self.clock_onsets.getTime()

        if in_time_code == 0:  # Response in time
            # Response imgs
            for i in range(self.objs.total_responses):
                self.objs.responses[i].img.draw()
            onsets["response_window"] = self.clock_onsets.getTime()
            self.psy.win.flip()

            # Take response
            ok_key, response_idx, response_time = events.get_response(self.control_keys)
            onsets["between_response_outcome"] = self.clock_onsets.getTime()
            in_time_code = events.check_response_in_time(response_time, self.min_response_time, self.max_response_time)

            if ok_key and in_time_code == 0:  # Response in time
                ok_response = True
                # Outcome img
                outcome_idx = self.s_r_o_hashmap[stim_idx][response_idx]
                points_earned, response_accuracy, outcome_not_selected_idx, max_poss_points = self.calculate_points_and_accuracy(
                    ok_response, outcome_idx, stim_idx=stim_idx)
                self.objs.stims[stim_idx].img.autoDraw = False
                self.draw_outcome_chosen(outcome_idx, response_accuracy, self.duration_between_response_outcome)

        events.draw_info_response_time(in_time_code, self.objs.outcome_text_fast, self.objs.outcome_text_slow)
        psychopy.event.clearEvents()
        self.objs.stims[stim_idx].img.autoDraw = False
        self.psy.win.flip()
        onsets["outcome_presentation"] = self.clock_onsets.getTime()
        psychopy.core.wait(self.duration_outcome_presentation)
        self.psy.win.flip()

        onsets["end_trial"] = self.clock_onsets.getTime()

        # Update results
        self.update_results(stim_idx, response_idx, outcome_idx, outcome_not_selected_idx, response_time,
                            in_time_code, points_earned, response_accuracy, max_poss_points, onsets)

    def consumption_trial(self, num_trial, show_countdown_text=False):
        self.trial_type = 1
        onsets = {}

        # Instructions
        self.objs.instr_consumption_trial.draw()
        self.psy.win.flip()
        psychopy.core.wait(self.duration_instr_consumption_trial)

        onsets["iti"] = self.wait_iti(num_trial, show_countdown_text)

        # Fixation
        self.objs.fixation_stim.draw()
        onsets["fixation"] = self.clock_onsets.getTime()
        self.psy.win.flip()
        psychopy.core.wait(self.duration_fixation_consumption_trial)
        psychopy.event.clearEvents()

        # Outcome objs
        possible_outcomes_inds = list(range(self.objs.total_outcomes))
        if self.outcome_dev_idx != -1:
            outcome_inds = list(np.random.choice(self.objs.o_max_points_indices, size=2, replace=False))
        else:
            outcome_inds = list(np.random.choice(len(possible_outcomes_inds), size=2, replace=False))

        self.objs.outcomes_change_pos_consumption(outcome_inds)

        # Arrow
        self.objs.arrows_stim.draw()
        onsets["stimulus"] = self.clock_onsets.getTime()
        self.psy.win.flip()
        psychopy.event.clearEvents()

        # Take response
        onsets["response_window"] = self.clock_onsets.getTime()
        ok_key, response_idx, response_time = events.get_response(self.control_keys)
        onsets["between_response_outcome"] = self.clock_onsets.getTime()
        in_time_code = events.check_response_in_time(response_time, self.min_response_time, self.max_response_consumption_trial)
        events.draw_info_response_time(in_time_code, self.objs.outcome_text_fast, self.objs.outcome_text_slow)

        # Restore outcome original pos
        self.objs.restore_outcomes_pos(outcome_inds)

        ok_response = False
        outcome_idx = -1
        points_earned = 0
        response_accuracy = 0
        outcome_not_selected_idx = -1
        max_poss_points = 0
        if in_time_code == 0:  # Response in time
            ok_response = True
            outcome_idx = outcome_inds[response_idx]
            points_earned, response_accuracy, outcome_not_selected_idx, max_poss_points = self.calculate_points_and_accuracy(
                ok_response, outcome_idx, outcome_inds=outcome_inds)
            self.draw_outcome_chosen(outcome_idx, response_accuracy, self.duration_between_response_outcome_consumption_trial)

        self.psy.win.flip()
        onsets["outcome_presentation"] = self.clock_onsets.getTime()
        psychopy.core.wait(self.duration_outcome_presentation)
        self.psy.win.flip()

        onsets["end_trial"] = self.clock_onsets.getTime()

        # Update results
        stim_idx = -1
        self.update_results(stim_idx, response_idx, outcome_idx, outcome_not_selected_idx, response_time,
                            in_time_code, points_earned, response_accuracy, max_poss_points, onsets)

    def wait_iti(self, num_trial, show_countdown_text):
        duration_iti = self.duration_iti_before_trial

        if self.outcome_dev_idx != -1:
            duration_iti = self.duration_iti_before_trial_dev
        if self.session_number == 3:
            duration_iti += np.random.uniform(0, self.duration_max_jitter)

        onset = self.clock_onsets.getTime()

        self.psy.win.flip()
        if show_countdown_text:
            self.show_countdown(duration_iti)
        else:
            psychopy.core.wait(duration_iti)
        psychopy.event.clearEvents()

        return onset

    def calculate_trials_stims_indices(self, num_trials_per_block, per_over_stim=0.5, times_over_stim=10.0,
                                       consumption_trials=True, outcome_dev_idx=-1):
        if num_trials_per_block < self.objs.total_stims:
            raise Exception("Error: num_trials_per_block ({}) must be greater than total number of stims ({})".format(
                num_trials_per_block, self.objs.total_stims))

        num_overtraining_stim, num_undertraining_stim = self.overtraining_stims_stats(per_over_stim)

        times_appear_overtraining_stim = times_over_stim * num_overtraining_stim  # Overtraining stim appear X times more than undertraining stim
        times_appear_undertraining_stim = 1 * num_undertraining_stim
        total_times_appear_stim = times_appear_overtraining_stim + times_appear_undertraining_stim

        mult_overtraining_num_stim = num_trials_per_block / total_times_appear_stim
        num_overtraining_stim_per_block = mult_overtraining_num_stim * times_appear_overtraining_stim
        num_overtraining_stim_per_block = math.floor(num_overtraining_stim_per_block / num_overtraining_stim)
        mult_undertraining_num_stim = num_trials_per_block / total_times_appear_stim
        num_undertraining_stim_per_block = mult_undertraining_num_stim * times_appear_undertraining_stim
        num_undertraining_stim_per_block = math.ceil(num_undertraining_stim_per_block / num_undertraining_stim)

        trials_stims_indices = []

        config_stims_per_block = [num_overtraining_stim_per_block] * num_overtraining_stim + \
                                 [num_undertraining_stim_per_block] * num_undertraining_stim

        for i, num_stim_per_block in enumerate(config_stims_per_block):
            stim_indices = [i] * num_stim_per_block
            trials_stims_indices += stim_indices
        random.shuffle(trials_stims_indices)

        # Check for no more than X consecutive repetitions of the same stimulus
        trials_stims_indices = self.shuffle_stims_with_no_max_consecutives(trials_stims_indices)

        if self.outcome_dev_idx == -1:  # No devaluation
            self.num_trials_per_block = len(trials_stims_indices)
        else:
            self.num_trials_per_block_dev = len(trials_stims_indices)

        # Consumption trials config:
        if consumption_trials:
            consumption_trials_inds = np.arange(self.consumption_trial_every, num_trials_per_block,
                                                step=self.consumption_trial_every)  # Consumption trial every X normal trials

            for i in range(len(trials_stims_indices)):
                if i in consumption_trials_inds:
                    trials_stims_indices.insert(i, -1)

        return trials_stims_indices

    def overtraining_stims_stats(self, per_over_stim):
        percentage_overtraining_stim = per_over_stim  # Overtraining stim are the X % of the total stim (e.g. if X=50% and there are 4 stimuli, 2 of them will be overtrained stim)
        num_overtraining_stim = int(self.objs.total_stims * percentage_overtraining_stim)
        num_undertraining_stim = self.objs.total_stims - num_overtraining_stim

        return num_overtraining_stim, num_undertraining_stim

    def shuffle_stims_with_no_max_consecutives(self, inds):
        are_max_consecutives = True
        i_consecutive = 1

        while are_max_consecutives:
            ind = 1
            while ind < len(inds):
                # print("Checking for max_consecutives in ind {}".format(ind))
                if inds[ind] == inds[ind-1]:
                    i_consecutive += 1
                else:
                    i_consecutive = 1

                if i_consecutive > self.max_consecutive_stims:  # Reallocate this 3rd consecutive stimulus to not be consecutive
                    # print("Trying to reallocate ind {}".format(ind))
                    insertion_possible = False
                    val_reallocate = inds[ind]
                    while not insertion_possible:
                        rand_ind = np.random.randint(0, len(inds))
                        insertion_possible = self.check_if_max_stim_consecutive_around(val_reallocate, rand_ind, inds)
                    val_substitute = inds[rand_ind]
                    inds[rand_ind] = val_reallocate
                    inds[ind] = val_substitute

                    # Reset search
                    ind = 1
                    i_consecutive = 1
                    # print("Resetting search for new_max because of new reallocations")
                else:
                    ind += 1

            are_max_consecutives = False

        return inds

    def check_if_max_stim_consecutive_around(self, val, ind, inds):
        insertion_possible = False

        start_ind = ind - self.max_consecutive_stims + 1 if ind >= self.max_consecutive_stims else 1
        end_ind = start_ind + (self.max_consecutive_stims * 2)
        if end_ind > len(inds):
            end_ind = len(inds)
        i_consecutive = 1
        for i in range(start_ind, end_ind):
            if inds[i] == val:
                i_consecutive += 1
            else:
                i_consecutive = 1

        if i_consecutive < self.max_consecutive_stims:
            insertion_possible = True

        return insertion_possible

    def calculate_blocks_config_indices(self):
        blocks_config_indices = []
        o_dev_indices = self.counterbalancing_o_dev()

        count_dev = 0
        for i, type in enumerate(self.blocks_design):
            dev_id = -1  # Training
            if type == "dev":
                if count_dev >= len(o_dev_indices):
                    count_dev = 0
                dev_id = o_dev_indices[count_dev]
                count_dev += 1

            blocks_config_indices.append(dev_id)

        # dev_block_inds_no_consec = []  # No definitve list, this not includes consecutive dev blocks
        # if self.dev_block_every > 0:
        #     dev_block_inds_no_consec = list(np.arange(self.dev_block_every, self.num_blocks, step=self.dev_block_every))
        #
        # dev_block_inds_no_consec = dev_block_inds_no_consec.copy()
        # dev_block_inds = []
        # for dev_block_idx in dev_block_inds_no_consec:
        #     dev_block_inds.append(dev_block_idx)
        #     for j in range(self.num_consecutive_dev_blocks):
        #         dev_block_inds.append(dev_block_idx+j+1)
        #
        # i_dev = 0
        # for i in range(self.num_blocks):
        #     outcome_dev_id = -1
        #     if i in dev_block_inds:
        #         if i_dev >= len(o_dev_indices):
        #             i_dev = 0
        #         outcome_dev_id = o_dev_indices[i_dev]
        #         i_dev += 1
        #     blocks_config_indices.append(outcome_dev_id)

        return blocks_config_indices

    def show_countdown(self, num_seconds=3):
        for i in reversed(range(num_seconds)):
            self.objs.countdown_info.text = str(i+1)
            self.objs.countdown_info.draw()
            self.psy.win.flip()
            psychopy.core.wait(1)

    def draw_outcome_chosen(self, outcome_idx, accuracy, delay):
        duration_between_response_outcome = delay

        if self.session_number == 3:
            duration_between_response_outcome += np.random.uniform(0, self.duration_max_jitter)

        self.black_screen_delay(duration_between_response_outcome)

        if self.outcome_dev_idx == -1:
            self.objs.outcomes[outcome_idx].img.draw()
            if self.trial_type == 1:  # consumption trial
                self.objs.outcomes[outcome_idx].points_obj.draw()
            else:  # Not a consumption trial
                self.objs.show_text_points_info(accuracy)
        else:
            self.objs.outcomes[outcome_idx].img.draw()
            self.objs.outcome_points_unknown.draw()

        return 0

    def calculate_points_and_accuracy(self, ok_response, outcome_idx, stim_idx=None, outcome_inds=[]):
        points_earned = self.objs.outcomes[outcome_idx].points if ok_response else 0
        if self.trial_type == 0:
            response_accuracy, outcome_not_selected_id, max_possible_points = self.accuracy_trial(
                stim_idx, points_earned)
        else:
            response_accuracy, outcome_not_selected_id, max_possible_points = self.accuracy_consumption_trial(
                outcome_inds, points_earned)

        return points_earned, response_accuracy, outcome_not_selected_id, max_possible_points

    def update_results(self, stim_idx, response_idx, outcome_idx, outcome_not_selected_idx, response_time, in_time_code,
                       points_earned, response_accuracy, max_possible_points, onsets):
        outcome_dev_selected = int(self.objs.outcomes[outcome_idx].is_dev)
        is_overtraining_stim = self.check_is_overtraining_stim(stim_idx)

        self.results["stim_id"].append(stim_idx)
        self.results["is_overtraining_stim"].append(is_overtraining_stim)
        self.results["response_id"].append(response_idx)
        self.results["response_accuracy"].append(response_accuracy)
        self.results["response_time"].append(response_time)
        self.results["response_in_time"].append(in_time_code)
        self.results["points_this_trial"].append(points_earned)
        self.results["points_max_possible_this_trial"].append(max_possible_points)
        self.results["outcome_selected_id"].append(outcome_idx)
        self.results["outcome_not_selected_id"].append(outcome_not_selected_idx)
        self.results["outcome_dev_selected"].append(outcome_dev_selected)
        self.results["onset_fixation"].append(onsets["fixation"])
        self.results["onset_stimulus"].append(onsets["stimulus"])
        self.results["onset_response_window"].append(onsets["response_window"])
        self.results["onset_between_response_outcome"].append(onsets["between_response_outcome"])
        self.results["onset_outcome_presentation"].append(onsets["outcome_presentation"])
        self.results["onset_iti"].append(onsets["iti"])
        self.results["onset_end_trial"].append(onsets["end_trial"])

        response_switch = 0
        if self.outcome_dev_idx != -1 and (outcome_not_selected_idx == self.outcome_dev_idx):
            response_switch = 1
        self.results["response_switch"].append(response_switch)

        self.set_total_points()

    def set_total_points(self):
        total_points = self.results["total_points"][-1] if len(self.results["total_points"]) > 0 else 0
        self.results["total_points"].append(total_points + self.results["points_this_trial"][-1])

    def accuracy_trial(self, stim_idx, points_earned):
        accuracy = 0
        max_possible_points = 0
        outcome_not_selected_id = -1

        if stim_idx == -1:
            return accuracy

        for response_idx, outcome_idx in self.s_r_o_hashmap[stim_idx].items():
            possible_outcome = self.objs.outcomes[outcome_idx].points
            if possible_outcome > max_possible_points:
                max_possible_points = possible_outcome
            if points_earned != possible_outcome:
                outcome_not_selected_id = outcome_idx

        if points_earned < max_possible_points:
            accuracy = 0  # Suboptimal response
        else:
            accuracy = 1  # Optimal response

        return accuracy, outcome_not_selected_id, max_possible_points

    def accuracy_consumption_trial(self, outcome_inds, points_earned):
        accuracy = 0
        max_possible_points = 0
        outcome_not_selected_id = -1

        for i, outcome_idx in enumerate(outcome_inds):
            possible_outcome = self.objs.outcomes[outcome_idx].points
            if possible_outcome > max_possible_points:
                max_possible_points = possible_outcome
            if points_earned != possible_outcome:
                outcome_not_selected_id = outcome_idx

        if points_earned < max_possible_points:
            accuracy = 0  # Suboptimal response
        else:
            accuracy = 1  # Optimal response

        return accuracy, outcome_not_selected_id, max_possible_points

    def check_is_overtraining_stim(self, stim_id):
        is_overtraining_stim = 0
        if stim_id in self.stim_overtraining_indices:
            is_overtraining_stim = 1

        return is_overtraining_stim

    def counterbalancing_o_dev(self):
        o_dev_indices = self.stim_overtraining_indices
        o_dev_indices_combs = list(itertools.permutations(o_dev_indices, len(o_dev_indices)))
        participant_number = self.participant_number
        if participant_number > len(o_dev_indices) - 1:
            participant_number = participant_number - \
                                 math.trunc((participant_number / len(o_dev_indices))) * len(o_dev_indices)

        o_dev_indices = list(o_dev_indices_combs[participant_number])

        return o_dev_indices

    def black_screen_delay(self, time):
        self.psy.win.flip(clearBuffer=True)
        psychopy.core.wait(time)

        return 0
