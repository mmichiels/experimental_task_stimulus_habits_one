from utils_psychopy import *


def get_response(allowed_keys=[], time_limit=False, time_to_wait_response=None):
    clock_response = core.Clock()  # Start timing response
    time_consumed = 0
    ok_key = False
    response_time = None
    response_idx = -1

    while True:
        keys_pressed = psychopy.event.getKeys()
        time_consumed = clock_response.getTime()

        if time_limit and time_consumed > time_to_wait_response:
            break

        if len(keys_pressed) == 1:
            this_key = keys_pressed[0]

            try:
                response_idx = allowed_keys.index(this_key)
                response_time = time_consumed
                ok_key = True
                break
            except ValueError:
                pass

    # psychopy.event.clearEvents()
    # print(ok_key, time_consumed)

    return ok_key, response_idx, response_time


def get_response_wait(allowed_keys=[]):
    clock_response = core.Clock()  # Start timing response
    ok_key = False
    response_time = None
    response_idx = -1

    while True:
        keys_pressed = psychopy.event.waitKeys()

        if len(keys_pressed) == 1:
            response_time = clock_response.getTime()
            this_key = keys_pressed[0]

            try:
                response_idx = allowed_keys.index(this_key)
                ok_key = True
                break
            except ValueError:
                pass

        psychopy.event.clearEvents()

    return ok_key, response_idx, response_time


def check_response_in_time(response_time, min_response_time, max_response_time):
    in_time_code = 0

    if response_time is not None:
        if response_time < min_response_time:
            in_time_code = 1
        elif response_time > max_response_time:
            in_time_code = 2

    # print(response_time, min_response_time)

    return in_time_code

def draw_info_response_time(in_time_code, obj_text_fast, obj_text_slow):
    if in_time_code == 1:
        obj_text_fast.draw()
    elif in_time_code == 2:
        obj_text_slow.draw()

    return 0