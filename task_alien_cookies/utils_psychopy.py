from __future__ import absolute_import, division

from datetime import datetime

import psychopy
from psychopy import sound, gui, visual, core, data, event, logging, clock

import numpy as np
import pandas as pd
import os  # handy system and path functions
import sys  # to get file system encoding

from psychopy.hardware import keyboard
import ctypes

import serial
import serial.tools.list_ports
import time

IS_WINDOWS_OS = sys.platform == "win32" or sys.platform == "cygwin"

class Psychopy:
    def __init__(self, debug, session_number, comp_model=False, in_fMRI=False):
        if debug and not IS_WINDOWS_OS:
            print("WARNING: Running in DEBUG mode. Turn DEBUG mode to False in utils_psychopy.py for production!")

        if not debug and not IS_WINDOWS_OS:
            try:
                xlib_name = "libX11.so"
                xlib = ctypes.cdll.LoadLibrary(xlib_name)
                xlib.XInitThreads()
            except Exception as e:
                print("WARNING: Couldn't load {} library, which is recommended. Proceeding anyways...".format(xlib_name))
                print(e)

        # Ensure that relative paths start from the same directory as this script
        _thisDir = os.path.dirname(os.path.abspath(__file__))
        os.chdir(_thisDir)

        self.is_comp_model = comp_model
        self.dir_images = os.path.join('.', 'images')
        self.dir_instructions_imgs = os.path.join(self.dir_images, 'instructions')
        self.dir_game_imgs = os.path.join(self.dir_images, 'game')
        self.dir_results = os.path.join('.', 'results')
        self.dir_logs = os.path.join('.', 'logs')
        self.results_file_prefix = "results_"
        self.participant_info_file_prefix = "info_"
        if not os.path.isdir(self.dir_results):
            os.mkdir(self.dir_results)

        # Store info about the experiment session
        psychopyVersion = '2020.1.0'
        expName = 'Alien-cookies'  # from the Builder filename that created this script

        self.exp_info = {}
        if debug:
            self.exp_info['session_number'] = 0
            self.exp_info['participant_number'] = 0
        if not debug:
            if in_fMRI:
                self.exp_info = {'participant_number': list(range(0, 23)),
                                 'age': list(range(0, 99)), 'gender': ['male', 'female', 'other'],
                                 'hand': ['right', 'left']}
            else:
                if session_number == 1:
                    self.exp_info = {'age': list(range(0, 99)),
                                     'gender': ['male', 'female', 'other'],
                                     'hand': ['right', 'left']}
                else:
                    self.exp_info = {'participant_number': list(range(0, 23)),
                                     'age': list(range(0, 99)),
                                     'gender': ['male', 'female', 'other'],
                                     'hand': ['right', 'left']}

            dlg = gui.DlgFromDict(dictionary=self.exp_info, sortKeys=False, title=expName)
            if not dlg.OK:
                core.quit()  # user pressed cancel
            if session_number == 1:
                self.exp_info['participant_number'] = int(np.random.randint(0, 23, 1)[0])  # range(0, 100)


        self.exp_info['in_fMRI'] = in_fMRI
        self.exp_info['session_number'] = session_number
        self.exp_info['time_pressure'] = 0
        self.exp_info['date'] = datetime.now().strftime('%Y_%m_%d_%H_%m_%S')  # add a simple timestamp
        self.exp_info['expName'] = expName
        self.exp_info['psychopyVersion'] = psychopyVersion

        if not self.is_comp_model:
            # Data file name stem = absolute path + name; later add .psyexp, .csv, .log, etc
            logs_filename = str(self.exp_info["participant_number"]) + \
                            "__" + self.exp_info['date']
            logs_filepath = os.path.join(self.dir_logs, logs_filename)
            if not os.path.isdir(self.dir_logs):
                os.mkdir(self.dir_logs)
            # save a log file for detail verbose info
            logFile = logging.LogFile(logs_filepath + '.log', level=logging.INFO)
            logging.console.setLevel(logging.WARNING)  # this outputs to the screen, not a file

            endExpNow = False  # flag for 'escape' or other condition => quit the exp
            frameTolerance = 0.001  # how close to onset before 'same' frame

            # Start Code - component code to be run before the window creation

            # Setup the Window
            self.win = visual.Window(
                size=[1920, 1080], fullscr=True, screen=1,
                winType='pyglet', allowGUI=False, allowStencil=False,
                monitor='testMonitor', color='black', colorSpace='rgb',
                blendMode='avg', useFBO=True,
                units='height')
            # store frame rate of monitor if we can measure it
            self.exp_info['frameRate'] = self.win.getActualFrameRate()
            if self.exp_info['frameRate'] != None:
                frameDur = 1.0 / round(self.exp_info['frameRate'])
            else:
                frameDur = 1.0 / 60.0  # could not measure, so guess

            # create a default keyboard (e.g. to check for escape)
            defaultKeyboard = keyboard.Keyboard()

            # Initialize components for Routine "instructions"
            instructionsClock = core.Clock()

        self.save_participant_info()

    def load_img_from_disk(self, img_filepath, pos=None, size=None):
        if self.is_comp_model:
            return None

        if pos is None:
            pos = (0, 0)
        if size is None:
            size = (1, 1)

        img = psychopy.visual.ImageStim(
            win=self.win,
            name='image',
            image=img_filepath, mask=None,
            ori=0, pos=pos, size=size,
            interpolate=True)

        return img

    def save_results(self, results, format="csv",  tmp=False):
        if format != "csv":
            raise Exception("Format {} not available".format(format))

        results_df = pd.DataFrame(results)
        results_filename = self.results_file_prefix + "_session_" + \
                           str(self.exp_info["session_number"]) + "__participant_" + \
                           str(self.exp_info["participant_number"]) + \
                           "__" + self.exp_info['date']
        self.save_csv(results_df, results_filename, format, tmp)

    def save_participant_info(self, format="csv",  tmp=False):
        participant_info_df = pd.Series(self.exp_info).to_frame().T
        participant_info_filename = self.participant_info_file_prefix + "_session_" + \
                                    str(self.exp_info["session_number"]) + "__participant_" + \
                                    str(self.exp_info["participant_number"]) + \
                                    "__" + self.exp_info['date']
        self.save_csv(participant_info_df, participant_info_filename, format, tmp)

    def save_csv(self, df, filename, format="csv", tmp=False):
        if tmp:
            filename += "_tmp"
        df_filepath = os.path.join(self.dir_results, filename) + "." + format
        df.to_csv(df_filepath)

def shift_list_n_times(list_input):
    # Shift all the values to the left n times

    comb = list_input.copy()
    combinations = [comb]
    for i in range(len(list_input) - 1):
        comb = comb.copy()
        comb.append(comb.pop(0))
        combinations.append(comb)

    return combinations