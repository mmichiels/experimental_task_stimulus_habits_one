## Project description
Instrumental learning task for testing habits VS goal-directed
 learning: Stimulus-response-outcome (with devalued outcomes
  for habits assessment).

This is the Python version in Psychopy 3 from the original experimental task designed
and coded by David Luque in Matlab with Psychtoolbox: https://github.com/DavLuke/MATLAB-Habits-OvertrainingExp1

Project for PhD UAM-HM CINAC.

--------------------------------
Original code by David Luque used (with detailed descriptions) in:

Luque, D., Molinero, S., Watson, P., López, F. J., & Le Pelley, M. E. (2019). Measuring Habit Formation Through Goal-Directed Response Switching. Journal of Experimental Psychology: General. https://doi.org/10.1037/xge0000722

Luque, D., Beesley, T., Morris, R. W., Jack, B. N., Griffiths, O., Whitford, T. J., & Le Pelley, M. E. (2017). Goal-directed and habit-like modulations of stimulus processing during reinforcement learning. Journal of Neuroscience, 37(11), 3009–3017. https://doi.org/10.1523/JNEUROSCI.3205-16.2017

--------------------------------

## Installation instructions

### Windows
1 - Download and install Pyschopy standalone package from:
https://www.psychopy.org/download.html

2 - Open Pyschopy. In the code viewer, open the file
```./task_alien_cookies/main_proc.py```

3 - Click the run button and in the new run experiments windows,
click again in the run button.

### Linux/Mac
1 - Download and install Anaconda:
https://docs.anaconda.com/anaconda/install/linux/

2 - Navigate to ```./task_alien_cookies```
```
cd ./task_alien_cookies
```

3 - Create conda environment with the conda_environment.yml
file with all the necessary packages:
```
conda env create -f conda_environment.yml
```

IF there are any errors with config-parser. Try installing first the conda environment only with Python. Then populate it with:

```
conda env update -f ./conda_environment.yml
```

4 - Activate the new conda environment:
```
conda activate conda_psycho_1_py367
```

4.1 - (Optional) In case of any update of the conda environment, run:
```
conda env update -f ./conda_environment.yml
```

5 - Run the file ```./task_alien_cookies/main_proc.py```
```
python main_proc.pywindows
```

## Important parameters
##### main_proc.py:

```debug```: True: no GUI for participants info and verbose output.
            False: GUI for participants info at the beggining and
             production mode recommended to run the experiment in a
              real environment.

```comp_model```: False: playing the game as human;
 True: playing the game as machine (computational model)

```instructions```: True: show game instructions at the beggining
of the game. It only has effect when ```comp_model=False```

##### Experiment.py:
At the beggining of this file, you will find the following parameters:

```self.num_blocks```, ```self.num_trials_per_block```,
```self.dev_block_every``` (insert a devaluation block every x blocks), etc.

Changing these parameters will cause the experiment to run a different
number of blocks and trials per block. For further detail, read the comments in the
code.
